//
//  sampleViewController.m
//  SearchFrameWorkSample
//
//  Created by vairat on 07/07/15.
//  Copyright (c) 2015 vaisoft. All rights reserved.
//

#import "sampleViewController.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "SearchViewController.h"
#import "DailyDealsViewController.h"
#import "NearByViewController.h"
#import "HomeViewController.h"
@interface sampleViewController ()
{
    ASIFormDataRequest *getUserDetailsRequest;
    UserDataXMLParser *parserDelegate;
}

@end

@implementation sampleViewController
@synthesize userID;
- (id)init {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    if ((self = [super initWithNibName:@"sampleViewController" bundle:bundle])) {
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSString *urlString = [NSString stringWithFormat:@"http://184.107.152.53/app/webroot/newapp/get_user.php?uid=%@",userID];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"url is %@",url);
    
    getUserDetailsRequest = [ASIFormDataRequest requestWithURL:url];
    [getUserDetailsRequest setDelegate:self];
    [getUserDetailsRequest startAsynchronous];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController.navigationBar setTranslucent:NO];
}

#pragma mark -- ASIHttpRequest Delegate methods

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    NSLog(@"RESULT in sampleVC: %@",[request responseString]);
    NSXMLParser *userDetailsParser = [[NSXMLParser alloc] initWithData:[request responseData]];
    parserDelegate = [[UserDataXMLParser alloc] init];
    parserDelegate.delegate = self;
    userDetailsParser.delegate = parserDelegate;
    [userDetailsParser parse];
    
    
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    NSLog(@"RESULT in sample:  %@",[request responseString]);
    
}

//===========UserDataXMLPARSER  DELEGATE METHODS =======================//

#pragma mark -- UserDataXMLParser Delegate methods

- (void)parsingUserDetailsFinished:(User *)userDetails
{
   
    
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.userId forKey:@"userId"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.client_id forKey:@"client_id"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.domain_id forKey:@"domain_id"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.type forKey:@"type"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.email forKey:@"email"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.first_name forKey:@"first_name"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.last_name forKey:@"last_name"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.state forKey:@"state"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.country forKey:@"country"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.mobile forKey:@"mobile"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.card_ext forKey:@"card_ext"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.client_name forKey:@"client_name"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.newsletter forKey:@"newsletter"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.clientDomainName forKey:@"clientDomainName"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
   
   // [[NSUserDefaults standardUserDefaults] stringForKey:@"client_id"]
    //[[NSUserDefaults standardUserDefaults] stringForKey:@"country"]
   
    NSLog(@"userId %@ ",[[NSUserDefaults standardUserDefaults] stringForKey:@"country"]);
    [self pushTosearchView];
    
}
-(void)pushTosearchView
{
    //NearByViewController *svc=[[NearByViewController alloc]init];
   // DailyDealsViewController *svc=[[DailyDealsViewController alloc]init];
  //  SearchViewController *svc=[[SearchViewController alloc]init];
      
    HomeViewController *svc=[[HomeViewController alloc]init];
    [[self navigationController] pushViewController:svc animated:YES];

}

- (void)userDetailXMLparsingFailed
{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
