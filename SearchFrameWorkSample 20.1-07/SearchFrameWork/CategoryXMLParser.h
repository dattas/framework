//
//  CategoryXMLParser.h
//  GrabItNow
//
//  Created by MyRewards on 12/22/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cat.h"

@protocol CategoryXMLParserDelegate;

@interface CategoryXMLParser : NSObject <NSXMLParserDelegate>
{
    __unsafe_unretained id <CategoryXMLParserDelegate> delegate;
}

@property (unsafe_unretained) id <CategoryXMLParserDelegate> delegate;

@end

@protocol CategoryXMLParserDelegate <NSObject>
- (void) parsingCategoriesFinished:(NSArray *) categoryList;
- (void) categoryXMLparsingFailed;
@end