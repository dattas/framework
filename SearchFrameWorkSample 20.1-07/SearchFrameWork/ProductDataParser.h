//
//  ProductDataParser.h
//  GrabItNow
//
//  Created by MyRewards on 12/25/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Product.h"

@protocol ProductXMLParserDelegate;

@interface ProductDataParser : NSObject <NSXMLParserDelegate> {
    __unsafe_unretained id <ProductXMLParserDelegate> delegate;
}

@property (unsafe_unretained) id <ProductXMLParserDelegate> delegate;

@end


@protocol ProductXMLParserDelegate <NSObject>

- (void) parsingProductDataFinished:(Product *) product;
- (void) parsingProductDataXMLFailed;

@end