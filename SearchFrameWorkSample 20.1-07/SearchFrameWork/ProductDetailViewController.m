//
//  ProductDetailViewController.m
//  GrabItNow
//
//  Created by vairat on 25/05/15.
//  Copyright (c) 2015 MyRewards. All rights reserved.
//
#define URL_Prefix @"http://www.myrewards.com.au/app/webroot/newapp/"//@"http://70.38.78.68/newapp/"// //
#define FORWARD  1
#define BACKWARD 2

#import "ProductDetailViewController.h"
#import "AppDelegate.h"
#import "DMLazyScrollView.h"
#import "NSString_stripHtml.h"
#import "MyCollectionViewCell.h"
#import "AddressCell.h"
#import "ASIFormDataRequest.h"
#import "AppDelegate.h"
#import "Merchant.h"
#import "Product.h"

@interface ProductDetailViewController () <DMLazyScrollViewDelegate>
{
    AppDelegate *appDelegate;
    DMLazyScrollView* lazyScrollView;
    NSMutableArray*    viewControllerArray;
    
    ASIFormDataRequest *imageRequest;
    ASIFormDataRequest *getProductRequest;
    ASIFormDataRequest *redeemstatusRequest;
    ASIFormDataRequest *redeemRequest;
    ASIFormDataRequest * merchantFetchRequest;
    //    RedeemXMLparser *redeemparser;
    ProductDataParser *productDataXMLParser;
    Product *currentProduct;
    //    Redeem *currentRedeem;
    
    UIWebView  *containerWebView;
    UIView  *contactsView;
    UITableView *addressTableView;
   
    //myCoupon
    UIView *myCouponBaseView;
    UIImageView *couponImageView;
    UILabel *proNameLabel;
    UILabel *merchantAddressLabel;
    UILabel *proDiscountLabel;
    UILabel *memNameLabel;
    UILabel *membershipLabel;
    UILabel *clientLabel;
    UIButton *redeemBtn;
    
    //myInfo
    UIView *myInfoBaseView;
    UIImageView *infoCouponImageView;
    UILabel *whatYouGetLabel;
    UILabel *proOfferLabel;
    UIView *desView;
    UILabel *offerDetailLabel;
    UILabel *redeemDetailLabel;
    UIView *termsAndConView;
    UILabel *addressDetailLabel;
    
    
    
    
    NSInteger currentBtnTag;
    UIButton *currentButton;
    UIButton *previousButton;
    NSInteger previousSelection;
    
    int contactLabelOrigin_Y;
    NSString *callNumber;
    
    int xOriginCouponImg, yOriginCouponImg, labelHeight, fontSize, redeemBtnHeight, extraSpace;
    
    NSMutableArray *merchantList;
    
    NSString *merchantAddString;
    
    Merchant *merchant;
    MerchantListParser *merchantListParser;
    NSBundle *bundle;
}

@end

@implementation ProductDetailViewController
@synthesize productCollectionView;
@synthesize cCell;
@synthesize favBtnOutlet, loadingView,product_ID;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    productCollectionView.dataSource = self;
    productCollectionView.delegate = self;
    
    [self.navigationController.navigationBar setTranslucent:NO];
  //  appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
   // [appDelegate.homeViewController hideBackButon:NO];
   
    
    previousButton = [[UIButton alloc]init];
    previousButton.tag = 1;
    currentBtnTag = 1;
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@get_product.php?id=%@",URL_Prefix,product_ID];
    NSLog(@"URL is %@ ",urlString);
    NSURL *url= [NSURL URLWithString:urlString];
    getProductRequest = [ASIFormDataRequest requestWithURL:url];
    [getProductRequest setDelegate:self];
    [getProductRequest startAsynchronous];
    
    [self fetchMerchantWithProductID:currentProduct.productId];
    
    
    NSBundle *bundle1 = [NSBundle bundleForClass:[self class]];
    [productCollectionView registerNib:[UINib nibWithNibName:@"MyCollectionViewCell" bundle:bundle1] forCellWithReuseIdentifier:@"CELL"];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
   // [appDelegate.homeViewController setHeaderTitle:@"Product Details"];
    self.title=@"Product";
    bundle=   [NSBundle bundleForClass:[self class]];
    [self setUpLoadingView];
}

- (void) viewDidAppear:(BOOL)animated
{
    
}

- (void)setUpLoadingView
{
    loadingView = [[UIView alloc]initWithFrame:self.view.bounds];
    loadingView.backgroundColor = [[UIColor grayColor]colorWithAlphaComponent:.7];
    [self.view addSubview:loadingView];
    
    [self activityIndicatorView];
}
-(void) activityIndicatorView
{
    CGRect frame = CGRectMake(self.view.bounds.size.width/2.5, self.view.bounds.size.height/2, 180, 40);
    UILabel *lblLoading = [[UILabel alloc]initWithFrame:frame];
    lblLoading.backgroundColor = [UIColor clearColor];
 
   [lblLoading setFont:[UIFont fontWithName:@"MyriadPro-Semibold"  size:17]];
    lblLoading.text = @"Loading...";
    lblLoading.textAlignment = NSTextAlignmentLeft;
    [lblLoading setTextColor:[UIColor blackColor]];
    
    [loadingView addSubview:lblLoading];
    
    CGRect frameActivity = CGRectMake(self.view.bounds.size.width/3.5, self.view.bounds.size.height/2, 40, 40);
    UIActivityIndicatorView  *myActivityIndicatorView = [[UIActivityIndicatorView alloc]initWithFrame:frameActivity];
    [loadingView addSubview:myActivityIndicatorView];
    
    [myActivityIndicatorView startAnimating];
    
}

- (void)pageSetting
{
    
    // PREPARE PAGES
    NSUInteger numberOfPages = 5;
    [viewControllerArray removeAllObjects];
    viewControllerArray = [[NSMutableArray alloc] initWithCapacity:numberOfPages];
    for (NSUInteger k = 0; k < numberOfPages; k++) {
        [viewControllerArray addObject:[NSNull null]];
    }
    
    // PREPARE LAZY VIEW
    lazyScrollView = Nil;
    lazyScrollView = [[DMLazyScrollView alloc] initWithFrame:CGRectMake(0, 0, productCollectionView.bounds.size.width, productCollectionView.bounds.size.height)];
    lazyScrollView.tag = 1222;
    
    lazyScrollView.dataSource = ^(NSUInteger index) {
        
        return [self controllerAtIndex:index];
        
        
    };
    
    lazyScrollView.numberOfPages = numberOfPages;
    lazyScrollView.controlDelegate = self;
    [productCollectionView.collectionViewLayout invalidateLayout];
}

-(void)setHighlightButtonImage:(NSInteger) index
{
    [productCollectionView.collectionViewLayout invalidateLayout];
    
    UIButton     *btnHeader333 = (UIButton *)[self.buttonsBaseView viewWithTag:previousButton.tag];
  
    [btnHeader333 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"btn%ld@2x.png",(long)previousButton.tag] inBundle:bundle compatibleWithTraitCollection:nil ] forState:UIControlStateNormal];
    NSLog(@"image path is btnHeader333 %@ ",btnHeader333 );
    UIButton     *btnHeader444 = (UIButton *)[self.buttonsBaseView viewWithTag:currentBtnTag];
    
     [btnHeader444 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"btnSelected%ld@2x.png",(long)currentBtnTag] inBundle:bundle compatibleWithTraitCollection:nil ] forState:UIControlStateNormal];
    previousButton.tag=currentBtnTag;
}

- (IBAction)headerBtnPressed:(id)sender {
    
    currentBtnTag = [sender tag];
    
    [productCollectionView.collectionViewLayout invalidateLayout];
    
    if(productCollectionView.contentOffset.y>0)
    {
        
        [productCollectionView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    
    currentButton = sender;
    
    [self setHighlightButtonImage:[sender tag]];
    
    if (previousSelection < [currentButton tag])
        [lazyScrollView setPage:[currentButton tag]-1 transition:FORWARD animated:NO];
    else
        [lazyScrollView setPage:[currentButton tag]-1 transition:BACKWARD animated:NO];
}

- (IBAction)favBtnPressed:(id)sender {
    
//    if (![appDelegate productExistsInFavorites:currentProduct])
//    {
//        //[appDelegate removeProductFromfavorites:currentProduct];
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Add to favorites?" message:[NSString stringWithFormat:@"Do you want to add %@ to favorites",currentProduct.productName] delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
//        
//        alert.tag = 21;
//        [alert show];
//    }
//    else {
//        //[appDelegate addProductToFavorites:currentProduct];
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Remove favorite?" message:[NSString stringWithFormat:@"Do you want to remove %@ from favorites",currentProduct.productName] delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
//        alert.tag = 22;
//        [alert show];
//    }
}

- (UIViewController *) controllerAtIndex:(NSInteger) index {
    
//    NSLog(@"---------------------controllerAtIndex %ld-------------------", (long)index);
    if (index > viewControllerArray.count || index < 0) return nil;
    
    id res = [viewControllerArray objectAtIndex:index];
    if (res == [NSNull null]) {
        
        UIViewController *contr = [[UIViewController alloc] init];
        contr.view.backgroundColor = [UIColor whiteColor];
        
        switch (index) {
                
                
            case 0:
                [contr.view addSubview:[self myInfoView]];
                
            
                break;
           
            case 1:
                
                containerWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0.0f,0.0f, productCollectionView.bounds.size.width, productCollectionView.bounds.size.height)];
                
                [contr.view addSubview:[self prepareTextToDisplay:currentProduct.productDesciption andHeadding:currentProduct.productOffer]];
                
                break;
                
            case 2:
                addressTableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, productCollectionView.bounds.size.width, productCollectionView.bounds.size.height) style:UITableViewStylePlain];
                addressTableView.backgroundColor = [UIColor whiteColor];
                addressTableView.dataSource = self;
                addressTableView.delegate = self;
                [contr.view addSubview:addressTableView];
                

                break;
            case 3:
                contactsView= [[UIView alloc] initWithFrame:CGRectMake(0.0f,0.0f, productCollectionView.bounds.size.width, 266.0f)];
                contactsView.backgroundColor = [UIColor whiteColor];
                [contr.view addSubview:contactsView];
                [self updateContactsView];
//                if([currentProduct.productTermsAndConditions length]>0)
//                    [contr.view addSubview:[self prepareTextToDisplay:currentProduct.productTermsAndConditions andHeadding:@"Terms And Conditions"]];
//                else
//                    [contr.view addSubview:[self prepareTextToDisplay:@"" andHeadding:@"No Terms And Conditions"]];
    
                break;
                
            case 4:
                
                [contr.view addSubview:[self myCouponView]];
                
                
                break;
                
            default:
                break;
        }
        
        
        
        
        [viewControllerArray replaceObjectAtIndex:index withObject:contr];
        return contr;
    }
    return res;
}


-(UIWebView *)prepareTextToDisplay:(NSString *)content andHeadding: (NSString *)header{
    
    containerWebView.userInteractionEnabled = NO;
    containerWebView.backgroundColor = [UIColor whiteColor];
    
    NSString *resultText = @"";
    
    resultText =       [NSString stringWithFormat:@"<html> \n"
                        
                        "<head> \n"
                        "<style type=\"text/css\"> \n"
                        "body {font-family: \"%@\"; font-size: \"%@ \";}\n"
                        "H4{ color: rgb(198,38,21) }\n"
                        "</style> \n"
                        "<style type='text/css'>body { max-width: 300%; width: auto; height: auto; }</style>"
                        "</head> \n"
                        "<body><H4>%@</H4>%@</body> \n"
                        "</html>", @"MyriadPro-Light", [NSNumber numberWithInt:10],header, content];

    

    NSLog(@"contentv ios %@",content);
    [containerWebView loadHTMLString:resultText baseURL:nil];
    return containerWebView;
}

-(UIWebView *)prepareTextToDisplay:(NSString *)content
{
    containerWebView.userInteractionEnabled = NO;
    containerWebView.backgroundColor = [UIColor whiteColor];
    
    NSString *resultText = @"";
    
    resultText =       [NSString stringWithFormat:@"<html> \n"
                        
                        "<head> \n"
                        "<style type=\"text/css\"> \n"
                        "body {font-family: \"%@\"; font-size:12;}\n"
                        "H4{ color: rgb(198,38,21) }\n"
                        "</style> \n"
                        "<style type='text/css'>body { max-width: 300%; width: auto; height: auto; }</style>"
                        "</head> \n"
                        "<body>%@</body> \n"
                        "</html>", @"MyriadPro-Light", content];
    
    [containerWebView loadHTMLString:resultText baseURL:nil];
    return containerWebView;
}

-(UIView *) myCouponView
{
   // [ [ UIScreen mainScreen ] bounds ].size.height == 568.0
    
    if ([ [ UIScreen mainScreen ] bounds ].size.height == 568.0) {
        xOriginCouponImg = productCollectionView.bounds.size.width/3.5;
        yOriginCouponImg = 15;
        labelHeight = 25;
        fontSize = 19;
        redeemBtnHeight = 50;
        extraSpace = 5;
    }
    else
    {
        xOriginCouponImg = productCollectionView.bounds.size.width/3;
        yOriginCouponImg = 7;
        labelHeight = 20;
        fontSize = 17;
        redeemBtnHeight = 45;
    }
    
    
    myCouponBaseView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, productCollectionView.bounds.size.width, productCollectionView.bounds.size.height)];
    
    couponImageView = [[UIImageView alloc]initWithFrame:CGRectMake(xOriginCouponImg, yOriginCouponImg, productCollectionView.bounds.size.height/3, productCollectionView.bounds.size.height/3)];
    couponImageView.layer.borderWidth = 5.0;
    couponImageView.layer.borderColor = [UIColor colorWithRed:189/255.0 green:194/255.0 blue:201/255.0 alpha:1.0].CGColor;
    
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:currentProduct.productImgLink]];//currProduct.productImgLink
    
    UIImage* image = [[UIImage alloc] initWithData:imageData];
    couponImageView.image = image;
    [myCouponBaseView addSubview:couponImageView];
    
    proNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, couponImageView.frame.origin.y + couponImageView.bounds.size.height + 7, productCollectionView.bounds.size.width - 10, 25)];
    NSLog(@"Available fonts: %@", [UIFont familyNames]);
     //NSLog(@"Available MyriadPro-Semibold fonts: %@", [NSString stringWithFormat:@"%@/MyriadPro-Semibold",[NSBundle bundleForClass:[self class]]]);
   
    [proNameLabel setFont:[UIFont fontWithName:@"MyriadPro-Semibold" size:21]];
    proNameLabel.text = currentProduct.productName;
    proNameLabel.textAlignment = NSTextAlignmentCenter;
    [proNameLabel setTextColor:[UIColor colorWithRed:87/255.0 green:102/255.0 blue:126/255.0 alpha:1.0]];
    [myCouponBaseView addSubview:proNameLabel];
    
    merchantAddressLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, proNameLabel.frame.origin.y + proNameLabel.bounds.size.height, productCollectionView.bounds.size.width - 10, labelHeight)];
    [merchantAddressLabel setFont:[UIFont fontWithName:@"MyriadPro-Semibold" size:13]];
    NSLog(@"merchant Address string is %@",merchantAddString);
//    merchantAddressLabel.text = @"6 Smith st, Collingwood VIC 3066";
//    merchantAddressLabel.text = merchantAddString;
    merchantAddressLabel.textAlignment = NSTextAlignmentCenter;
    [merchantAddressLabel setTextColor:[UIColor colorWithRed:87/255.0 green:102/255.0 blue:126/255.0 alpha:1.0]];
    [myCouponBaseView addSubview:merchantAddressLabel];
    
    proDiscountLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, merchantAddressLabel.frame.origin.y + merchantAddressLabel.bounds.size.height + 10 + extraSpace, productCollectionView.bounds.size.width -10, labelHeight)];
    [proDiscountLabel setFont:[UIFont fontWithName:@"MyriadPro-Semibold" size:23]];
    proDiscountLabel.text = currentProduct.productOffer;
    proDiscountLabel.textAlignment = NSTextAlignmentCenter;
    [proDiscountLabel setTextColor:[UIColor colorWithRed:87/255.0 green:102/255.0 blue:126/255.0 alpha:1.0]];
    [myCouponBaseView addSubview:proDiscountLabel];
    
    memNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, proDiscountLabel.frame.origin.y + proDiscountLabel.bounds.size.height + 10 + extraSpace, productCollectionView.bounds.size.width - 10, labelHeight)];
    [memNameLabel setFont:[UIFont fontWithName: @"MyriadPro-Semibold" size:fontSize]];
    memNameLabel.text =[NSString stringWithFormat:@"Name: %@ ",[[NSUserDefaults standardUserDefaults] stringForKey:@"last_name"]];
    memNameLabel.textAlignment = NSTextAlignmentCenter;
    [memNameLabel setTextColor:[UIColor colorWithRed:87/255.0 green:102/255.0 blue:126/255.0 alpha:1.0]];
    [myCouponBaseView addSubview:memNameLabel];
    
     membershipLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, memNameLabel.frame.origin.y + memNameLabel.bounds.size.height, productCollectionView.bounds.size.width -10, labelHeight)];
    [membershipLabel setFont:[UIFont fontWithName:@"MyriadPro-Semibold" size:fontSize]];
    membershipLabel.text =  [NSString stringWithFormat:@"Membership: %@", [[NSUserDefaults standardUserDefaults] stringForKey:@"first_name"]];
    membershipLabel.textAlignment = NSTextAlignmentCenter;
    [membershipLabel setTextColor:[UIColor colorWithRed:87/255.0 green:102/255.0 blue:126/255.0 alpha:1.0]];
    [myCouponBaseView addSubview:membershipLabel];
    
    clientLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, membershipLabel.frame.origin.y + membershipLabel.bounds.size.height, productCollectionView.bounds.size.width -10, labelHeight)];
    [clientLabel setFont:[UIFont fontWithName:@"MyriadPro-Semibold" size:fontSize]];
   /*
    
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.userId forKey:@"userId"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.client_id forKey:@"client_id"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.domain_id forKey:@"domain_id"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.type forKey:@"type"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.email forKey:@"email"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.first_name forKey:@"first_name"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.last_name forKey:@"last_name"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.state forKey:@"state"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.country forKey:@"country"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.mobile forKey:@"mobile"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.card_ext forKey:@"card_ext"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.client_name forKey:@"client_name"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.newsletter forKey:@"newsletter"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.clientDomainName forKey:@"clientDomainName"];
    
    */
    
    
    
    //[[NSUserDefaults standardUserDefaults] stringForKey:@"first_name"]
    //[[NSUserDefaults standardUserDefaults] stringForKey:@"country"]
    
   clientLabel.text = [NSString stringWithFormat:@"Client: %@", [[NSUserDefaults standardUserDefaults] stringForKey:@"client_name"]];
    
    redeemBtn.titleLabel.font = [UIFont fontWithName:@"MyriadPro-Semibold" size:19];
    
    clientLabel.textAlignment = NSTextAlignmentCenter;
    [clientLabel setTextColor:[UIColor colorWithRed:87/255.0 green:102/255.0 blue:126/255.0 alpha:1.0]];
    
    NSLog(@"clientLabel is %@ ",clientLabel);
    [myCouponBaseView addSubview:clientLabel];
    
    redeemBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    redeemBtn.frame = CGRectMake(couponImageView.frame.origin.x+(couponImageView.frame.size.width -135)/2 , clientLabel.frame.origin.y + clientLabel.bounds.size.height + 10 + extraSpace, 135, redeemBtnHeight);
    redeemBtn.backgroundColor = [UIColor colorWithRed:235/255.0 green:0/255.0 blue:139/255.0 alpha:1.0];
    redeemBtn.titleLabel.font = [UIFont fontWithName:@"MyriadPro-Semibold" size:19];
    [redeemBtn setTitle:@"REDEEM NOW" forState:UIControlStateNormal];
    [redeemBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [redeemBtn addTarget:self action:@selector(redeemBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [myCouponBaseView addSubview:redeemBtn];
    
    return myCouponBaseView;
}

-(UIView *) myInfoView
{
    
    if ([ [ UIScreen mainScreen ] bounds ].size.height == 568.0) {
        xOriginCouponImg = productCollectionView.bounds.size.width/3.5;
        yOriginCouponImg = 15;
        extraSpace = 5;
    }
    else
    {
        xOriginCouponImg = productCollectionView.bounds.size.width/3;
        yOriginCouponImg = 7;
    }
    
    
    myInfoBaseView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, productCollectionView.bounds.size.width, productCollectionView.bounds.size.height)];
    
    infoCouponImageView = [[UIImageView alloc]initWithFrame:CGRectMake(xOriginCouponImg, yOriginCouponImg, productCollectionView.bounds.size.height/3, productCollectionView.bounds.size.height/4)];
    infoCouponImageView.layer.borderWidth = 5.0;
    infoCouponImageView.layer.borderColor = [UIColor colorWithRed:189/255.0 green:194/255.0 blue:201/255.0 alpha:1.0].CGColor;
    
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:currentProduct.productImgLink]];//currProduct.productImgLink
    UIImage* image = [[UIImage alloc] initWithData:imageData];
    infoCouponImageView.image = image;
    
    [myInfoBaseView addSubview:infoCouponImageView];
    
    whatYouGetLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, infoCouponImageView.frame.origin.y + infoCouponImageView.bounds.size.height + 7 + extraSpace, productCollectionView.bounds.size.width/2.5, 30)];
    whatYouGetLabel.layer.borderWidth = 1.0;
    whatYouGetLabel.layer.borderColor = [UIColor lightGrayColor].CGColor;
   
   [whatYouGetLabel setFont:[UIFont fontWithName:@"MyriadPro-Light" size:17]];
    whatYouGetLabel.text = @"What you get...";
    whatYouGetLabel.textAlignment = NSTextAlignmentCenter;
    [whatYouGetLabel setTextColor:[UIColor blackColor]];
    [myInfoBaseView addSubview:whatYouGetLabel];
    
//    proDiscountLabel = [[UILabel alloc]init];
    proDiscountLabel = [[UILabel alloc]initWithFrame:CGRectMake(whatYouGetLabel.frame.origin.x + whatYouGetLabel.bounds.size.width +15 , infoCouponImageView.frame.origin.y + infoCouponImageView.bounds.size.height + 7 + extraSpace, productCollectionView.bounds.size.width/2, 30)];
    
    [proDiscountLabel setFont:[UIFont fontWithName:@"MyriadPro-Semibold" size:15]];
    proDiscountLabel.text = currentProduct.productOffer;
    proDiscountLabel.textAlignment = NSTextAlignmentLeft;
//    proDiscountLabel.backgroundColor = [UIColor redColor];
//    CGFloat width =  [proDiscountLabel.text sizeWithFont:[UIFont fontWithName:@"MyriadPro-Semibold" size:15 ]].width;
//    proDiscountLabel.frame = CGRectMake(whatYouGetLabel.frame.origin.x + whatYouGetLabel.bounds.size.width +15, infoCouponImageView.frame.origin.y + infoCouponImageView.bounds.size.height + 7, width+40,  30);
    
    
//    [proDiscountLabel sizeToFit];
    
    [proDiscountLabel setTextColor:[UIColor blackColor]];
    [myInfoBaseView addSubview:proDiscountLabel];
    
    proNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, whatYouGetLabel.frame.origin.y + whatYouGetLabel.bounds.size.height + 5, productCollectionView.bounds.size.width -20, 25)];
    
    [proNameLabel setFont:[UIFont fontWithName:@"MyriadPro-Light" size:17]];
    proNameLabel.text = currentProduct.productName;
    proNameLabel.textAlignment = NSTextAlignmentLeft;
    [proNameLabel setTextColor:[UIColor blackColor]];
    [myInfoBaseView addSubview:proNameLabel];
    
    proOfferLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, proNameLabel.frame.origin.y + proNameLabel.bounds.size.height + 2 +extraSpace, productCollectionView.bounds.size.width -15, 20)];
    
    [proOfferLabel setFont:[UIFont fontWithName:@"MyriadPro-Light"  size:14]];
    proOfferLabel.text = currentProduct.productOffer;
    proOfferLabel.textAlignment = NSTextAlignmentLeft;
    [proOfferLabel setTextColor:[UIColor blackColor]];
    [myInfoBaseView addSubview:proOfferLabel];
    
    desView = [[UIView alloc]initWithFrame:CGRectMake(15, proOfferLabel.frame.origin.y + proOfferLabel.bounds.size.height , productCollectionView.bounds.size.width -20, infoCouponImageView.bounds.size.height/1.5)];
    [myInfoBaseView addSubview:desView];
    
    NSLog(@"wv height is %f",infoCouponImageView.bounds.size.height/2);
    containerWebView = [[UIWebView alloc] initWithFrame:CGRectMake(-10.0f,0.0f, productCollectionView.bounds.size.width , infoCouponImageView.bounds.size.height/1.5)];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(webViewTap)];
    tap.numberOfTapsRequired = 1;
    
    [desView addSubview:[self prepareTextToDisplay:currentProduct.productDesciption]];
    
    [desView addGestureRecognizer:tap];
    
    UILabel *offerLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, desView.frame.origin.y + desView.bounds.size.height + extraSpace*2, productCollectionView.bounds.size.width/3, 20)];
    offerLabel.layer.borderWidth = 1.0;
    offerLabel.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [offerLabel setFont:[UIFont fontWithName:@"MyriadPro-Light"  size:12]];
    offerLabel.text = @"OFFER";
    offerLabel.textAlignment = NSTextAlignmentCenter;
    [offerLabel setTextColor:[UIColor blackColor]];
    [myInfoBaseView addSubview:offerLabel];
    
    offerDetailLabel = [[UILabel alloc]initWithFrame:CGRectMake(offerLabel.frame.origin.x + offerLabel.bounds.size.width + 10 , desView.frame.origin.y + desView.bounds.size.height + extraSpace *2, productCollectionView.bounds.size.width/1.8, 20)];
    [offerDetailLabel setFont:[UIFont fontWithName:@"MyriadPro-Light" size:13]];
    offerDetailLabel.text = currentProduct.productOffer;
    offerDetailLabel.textAlignment = NSTextAlignmentLeft;
    [offerDetailLabel setTextColor:[UIColor blackColor]];
    [myInfoBaseView addSubview:offerDetailLabel];
    
    UILabel *howToRedeemLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, offerLabel.frame.origin.y + offerLabel.bounds.size.height + 4, productCollectionView.bounds.size.width/3, 20)];
    howToRedeemLabel.layer.borderWidth = 1.0;
    howToRedeemLabel.layer.borderColor = [UIColor lightGrayColor].CGColor;
     [howToRedeemLabel setFont:[UIFont fontWithName:@"MyriadPro-Light"  size:12]];
    howToRedeemLabel.text = @"HOW TO REDEEM";
    howToRedeemLabel.textAlignment = NSTextAlignmentCenter;
    [howToRedeemLabel setTextColor:[UIColor blackColor]];
    [myInfoBaseView addSubview:howToRedeemLabel];
    
    redeemDetailLabel = [[UILabel alloc]initWithFrame:CGRectMake(howToRedeemLabel.frame.origin.x + howToRedeemLabel.bounds.size.width + 10, offerLabel.frame.origin.y + offerLabel.bounds.size.height + 4, productCollectionView.bounds.size.width/1.8, 20)];
    
    [redeemDetailLabel setFont:[UIFont fontWithName:@"MyriadPro-Light"  size:13]];
    redeemDetailLabel.text = @"Print or present coupon to redeem the offer.";
    redeemDetailLabel.textAlignment = NSTextAlignmentLeft;
    [redeemDetailLabel setTextColor:[UIColor blackColor]];
    [myInfoBaseView addSubview:redeemDetailLabel];

    
    UILabel *tAndcLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, howToRedeemLabel.frame.origin.y + howToRedeemLabel.bounds.size.height + 4, productCollectionView.bounds.size.width/3, 20)];
    tAndcLabel.layer.borderWidth = 1.0;
    tAndcLabel.layer.borderColor = [UIColor lightGrayColor].CGColor;
     [tAndcLabel setFont:[UIFont fontWithName:@"MyriadPro-Light" size:12]];
    tAndcLabel.text = @"T&C's";
    tAndcLabel.textAlignment = NSTextAlignmentCenter;
    [tAndcLabel setTextColor:[UIColor blackColor]];
    [myInfoBaseView addSubview:tAndcLabel];
    
    termsAndConView = [[UIView alloc]initWithFrame:CGRectMake(tAndcLabel.frame.origin.x + tAndcLabel.bounds.size.width + 10, howToRedeemLabel.frame.origin.y + howToRedeemLabel.bounds.size.height + 4, productCollectionView.bounds.size.width/1.8, 20)];
    
    [myInfoBaseView addSubview:termsAndConView];
    
    containerWebView = [[UIWebView alloc] initWithFrame:CGRectMake(-10.0f, -5.0f, productCollectionView.bounds.size.width/1.8 , 20)];
    
    if([currentProduct.productTermsAndConditions length]>0)
        [termsAndConView addSubview:[self prepareTextToDisplay:currentProduct.productTermsAndConditions]];
    else
        [termsAndConView addSubview:[self prepareTextToDisplay:@"No Terms And Conditions"]];

    
    
    UILabel *addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, termsAndConView.frame.origin.y + termsAndConView.bounds.size.height + 4, productCollectionView.bounds.size.width/3, 20)];
    addressLabel.layer.borderWidth = 1.0;
    addressLabel.layer.borderColor = [UIColor lightGrayColor].CGColor;
     [addressLabel setFont:[UIFont fontWithName:@"MyriadPro-Light" size:12]];
    addressLabel.text = @"ADDRESS";
    addressLabel.textAlignment = NSTextAlignmentCenter;
    [addressLabel setTextColor:[UIColor blackColor]];
    [myInfoBaseView addSubview:addressLabel];
    
    addressDetailLabel = [[UILabel alloc]initWithFrame:CGRectMake(addressLabel.frame.origin.x + addressLabel.bounds.size.width + 10, tAndcLabel.frame.origin.y + tAndcLabel.bounds.size.height + 4, productCollectionView.bounds.size.width/1.8, 20)];
    [addressDetailLabel setFont:[UIFont fontWithName:[NSString stringWithFormat:@"%@/MyriadPro-Light",bundle] size:13]];
//    addressDetailLabel.text = @"6 Smith st, Collingwood VIC 3066";
    addressDetailLabel.textAlignment = NSTextAlignmentLeft;
    [addressDetailLabel setTextColor:[UIColor blackColor]];
    [myInfoBaseView addSubview:addressDetailLabel];
    
    return myInfoBaseView;
}

-(void) webViewTap {
    NSLog(@"tap");
    
    currentBtnTag = 2;
    [self setHighlightButtonImage:currentBtnTag];
    [lazyScrollView setPage:1 transition:FORWARD animated:NO];
}

- (IBAction)redeemBtnPressed:(id)sender
{
    NSLog(@"Redeem btn Pressed");
    
    UIAlertView *redemAlert = [[UIAlertView alloc]initWithTitle:@"The redeem button is for merchant use only." message:@"A merchant  will press this to record your redemption. Some offers/vouchers do not permit multiple use, so don't waste a voucher.\n\n" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    
    
    UILabel *txtField = [[UILabel alloc] initWithFrame:CGRectMake(12.0, 160.0, 260.0, 45.0)];
  //  [txtField setFont:[UIFont fontWithName:@"Helvetica-Bold" size:(15.0)]];
    txtField.textAlignment = UITextAlignmentCenter;
    txtField.numberOfLines = 2;
    txtField.textColor = [UIColor whiteColor];
    txtField.text = @"If you are not a merchant, Press No now to go back.";
    txtField.backgroundColor = [UIColor clearColor];
    [redemAlert addSubview:txtField];
    redemAlert.tag = 222;
    [redemAlert show];
}

#pragma mark - lazyScrollView Delegate Methods

- (void)lazyScrollViewDidEndDecelerating:(DMLazyScrollView *)pagingView atPageIndex:(NSInteger)pageIndex
{
    
    
    UIButton *button=[[UIButton alloc]init];
    button.tag=pageIndex+1;
    
    [self performSelector:@selector(headerBtnPressed:) withObject:button afterDelay:0.03];
    
}



- (void)lazyScrollViewDidEndDragging:(DMLazyScrollView *)pagingView{
    
    [productCollectionView.collectionViewLayout invalidateLayout];
    
}


- (void)lazyScrollView:(DMLazyScrollView *)pagingView currentPageChanged:(NSInteger)currentPageIndex{
    
    
     NSLog(@"currentPageIndex::%ld", (long)currentPageIndex);
    currentBtnTag = currentPageIndex+1;
   
    // NSLog(@"lazyscroll view width is %f",pagingView.bounds.size.width);
}

- (void) updateContactsView {
    
    NSString* phnoString = [currentProduct.phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    phArray = [[NSArray alloc]init];
    phMutableArray = [[NSMutableArray alloc]init];
    
    //    phArray = [phnoString componentsSeparatedByString:@"/,"];
    phArray = [phnoString componentsSeparatedByCharactersInSet:
               [NSCharacterSet characterSetWithCharactersInString:@"/,"]
               ];
    NSLog(@"strings %@",phArray);
    
    if([phArray count] !=  0)
    {
        contactLabelOrigin_Y = 10;
        for (int i = 0; i < [phArray count]; i++) {
            
            UILabel *contactlabel = [[UILabel alloc]initWithFrame:CGRectMake(70, contactLabelOrigin_Y +3, 150, 30)];
            [contactlabel setFont:[UIFont fontWithName:[NSString stringWithFormat:@"%@/MyriadPro-Light",bundle] size:17]];
            [contactsView addSubview:contactlabel];
            
            if ([[phArray objectAtIndex:i] length] <= 4) {
                
                NSMutableArray *characters = [[NSMutableArray alloc] initWithCapacity:[[phArray objectAtIndex:i-1] length] - [[phArray objectAtIndex:i] length]];
                
                for (int j=0; j < [[phArray objectAtIndex:i-1] length] - [[phArray objectAtIndex:i] length]; j++) {
                    NSString *ichar  = [NSString stringWithFormat:@"%c", [[phArray objectAtIndex:i-1] characterAtIndex:j]];
                    [characters addObject:ichar];
                }
                
                NSString * newString = [[characters valueForKey:@"description"] componentsJoinedByString:@""];
                contactlabel.text = [newString stringByAppendingString:[phArray objectAtIndex:i]];
                
                [phMutableArray addObject:contactlabel.text];
            }
            else
            {
                contactlabel.text = [phArray objectAtIndex:i];
                [phMutableArray addObject:[phArray objectAtIndex:i]];
            }
            
           // @"btnSelected4@2x.png"
            UIButton *contactlabel_Button = [[UIButton alloc]initWithFrame:CGRectMake(30, contactLabelOrigin_Y, 35, 35)];
            contactlabel_Button.tag = i;
            NSLog(@"contact btn tag %ld",(long)[contactlabel_Button tag]);
            [contactlabel_Button setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@/btnSelected4@2x.png",bundle]] forState:UIControlStateNormal];
            [contactsView addSubview:contactlabel_Button];
            
            
            [contactlabel_Button addTarget:self action:@selector(callButtonpressed:)forControlEvents:UIControlEventTouchUpInside];
            contactlabel_Button.userInteractionEnabled = YES;
            
            
            
            //            [contactBtn setBackgroundImage:[UIImage imageNamed:@"btn3.png"] forState:UIControlStateNormal];
            
            
            
            contactLabelOrigin_Y = contactLabelOrigin_Y + 35;
        }
        
    }
    else
    {
        UILabel *contactsLabel  = [[UILabel alloc]initWithFrame:CGRectMake(60, 5, 150, 50)];
        
        contactsLabel.text = @"No contact Number Available";
        
        contactsLabel.numberOfLines = 2;
         [contactsLabel setFont:[UIFont fontWithName:[NSString stringWithFormat:@"MyriadPro-Light"] size:17]];
        [contactsView addSubview:contactsLabel];
        
    }
    
    
    // website button and label
    if([currentProduct.websiteLink length] > 0)
    {
        UILabel *websiteLabel;
        UIButton *websiteLabel_Button;
        
        if([phArray count] ==1){
            websiteLabel  = [[UILabel alloc]initWithFrame:CGRectMake(70, 55, 245, 30)];
            websiteLabel_Button = [[UIButton alloc]initWithFrame:CGRectMake(70, 55, 245, 30)];}
        else{
            websiteLabel  = [[UILabel alloc]initWithFrame:CGRectMake(70,75, 245, 30)];
            websiteLabel_Button = [[UIButton alloc]initWithFrame:CGRectMake(70, 75, 245, 30)];
        }
        [websiteLabel_Button addTarget:self
                                action:@selector(websiteButtonpressed:)
                      forControlEvents:UIControlEventTouchUpInside];
        
        websiteLabel.text = currentProduct.websiteLink;
        
         [websiteLabel setFont:[UIFont fontWithName:[NSString stringWithFormat:@"%@/MyriadPro-Light",bundle] size:15]];
        [contactsView addSubview:websiteLabel];
        [contactsView addSubview:websiteLabel_Button];
        
        UIButton *websiteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [websiteButton setImage:[UIImage imageNamed:@"globe.png"] forState:UIControlStateNormal];
        [websiteButton addTarget:self
                          action:@selector(websiteButtonpressed:)
                forControlEvents:UIControlEventTouchUpInside];
        if([phArray count] ==1)
            websiteButton.frame = CGRectMake(35.0,60.0, 25.0, 25.0);
        
        else
            websiteButton.frame = CGRectMake(30.0,80.0, 25.0, 25.0);
        [contactsView addSubview:websiteButton];
        
    }

    
}
-(IBAction)callButtonpressed:(id)sender
{
    NSLog(@"call button pressed");
    
    
    callNumber = [phMutableArray objectAtIndex:[sender tag]];
    
    UIAlertView *call_Alert = [[UIAlertView alloc]initWithTitle:@"Call" message:callNumber delegate:self cancelButtonTitle:@"Call" otherButtonTitles:@"Cancel" , nil];
    call_Alert.tag = 111;
    [call_Alert show];
    
}

-(void)websiteButtonpressed:(id)sender
{
    NSLog(@"==website button pressed");
    NSURL *websiteUrl = [NSURL URLWithString:currentProduct.websiteLink];
    
    [[UIApplication sharedApplication] openURL:websiteUrl];
}

#pragma mark - CollectionView DataSource And Delegate Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 1;
}

- (MyCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
        MyCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
   

    [cell addSubview:lazyScrollView];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    

    
    
    switch (currentBtnTag) {
        case 1:
            return CGSizeMake(productCollectionView.bounds.size.width, productCollectionView.bounds.size.height);
            break;
        case 2:
            
            return CGSizeMake(productCollectionView.bounds.size.width, productCollectionView.bounds.size.height);
            
            break;
        case 3:
            
            return CGSizeMake(productCollectionView.bounds.size.width, productCollectionView.bounds.size.height);
            
            break;
        case 4:
            
            return CGSizeMake(productCollectionView.bounds.size.width, productCollectionView.bounds.size.height);
            
            break;
        case 5:
            
            
            return CGSizeMake(productCollectionView.bounds.size.width, productCollectionView.bounds.size.height);
            
            break;
        case 6:
            
            
            return CGSizeMake(productCollectionView.bounds.size.width, productCollectionView.bounds.size.height);
            
            break;
    }

      return CGSizeMake(0, 0);
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark- Request Finished and Request Failed

- (void)requestFinished:(ASIHTTPRequest *)request
{
    if(request == getProductRequest){
        NSLog(@"=======>> Product:: %@",[request responseString]);
        NSXMLParser *productParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        productDataXMLParser = [[ProductDataParser alloc] init];
        productDataXMLParser.delegate = self;
        productParser.delegate = productDataXMLParser;
        [productParser parse];
        
    }
    
    else if (request == redeemRequest)
    {
        redeemRequest = nil;
//        [self.redeemActivityIndicator stopAnimating];
        NSLog(@"REDEEM RESPONSE::: %@",[request responseString]);
        
        NSString *msg = [NSString stringWithFormat:@"For redeeming this offer %@",currentProduct.productName];
        if([[request responseString] isEqualToString:@"success"])
        {
            
            UIAlertView *thanksAlert = [[UIAlertView alloc]initWithTitle:@"Thank You!" message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil, nil];
            thanksAlert.tag = 333;
            [thanksAlert show];
            
        }

    }
    
    else  {
         NSLog(@"Merchant ** RES: %@",[request responseString]);
        
        NSXMLParser *merchantParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        merchantListParser = [[MerchantListParser alloc] init];
        merchantListParser.delegate = self;
        merchantParser.delegate = merchantListParser;
        [merchantParser parse];
        
        merchantFetchRequest = nil;
        // addressDetailsLoaded = YES;
    }
    
    
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    UIAlertView* alert_view = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                         message:@"Server Busy"
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:@"Cancel",nil];
    [alert_view show];
    
//    if(request == redeemRequest)
//    {
//        redeemRequest = nil;
//        //[self.redeemActivityIndicator stopAnimating];
//        NSLog(@"REDEEM RESPONSE::: %@",[request responseString]);
//        UIAlertView *failedAlert=[[UIAlertView alloc]initWithTitle:@"Failed!" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [failedAlert show];
//    }
    
}


#pragma mark- fetch Address

- (void) fetchMerchantWithProductID:(NSString *) productId {
    
    if (merchantFetchRequest) {
        // Already a request is in progress.
        return;
    }
    
    
    // Make server call for more products.
    NSString *urlString = [NSString stringWithFormat:@"%@get_product_addresses.php?pid=%@",URL_Prefix,product_ID];
    
    NSLog(@"Fetch Merchants URL: %@",urlString);
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    merchantFetchRequest = [ASIFormDataRequest requestWithURL:url];
    [merchantFetchRequest setDelegate:self];
    [merchantFetchRequest startAsynchronous];
    
    
}

#pragma mark- Parsing Methods

- (void)parsingProductDataFinished:(Product *)product
{
    NSLog(@"product is %@ %@",product,product.productTermsAndConditions);
    currentProduct = product;

    
    
    [self pageSetting];
    [lazyScrollView setPage:0 transition:FORWARD animated:NO];
    
    [productCollectionView reloadData];
    [self updateFavoriteStatus];
    
    [loadingView removeFromSuperview];
}

- (void)parsingProductDataXMLFailed
{
    
}

- (void) RedeemXMLparsingFailed{}

-(void)redeemStatusRequestMethod{
    
//    NSString *rdmurl =[NSString stringWithFormat:@"%@redeem_status.php?user_id=%@&pid=%@",URL_Prefix,[self deviceUDID],currentProduct.productId];
//    NSURL *redeemurl= [NSURL URLWithString:rdmurl];
//    redeemstatusRequest=[ASIFormDataRequest requestWithURL:redeemurl];
//    [redeemstatusRequest setDelegate:self];
//    [redeemstatusRequest startAsynchronous];
    
    
}

- (void)parsingMerchantListFinished:(NSArray *)merchantsListLocal {
    
    
    if (!merchantList)
        merchantList = [[NSMutableArray alloc] initWithArray:merchantsListLocal];
    
    else
        [merchantList addObjectsFromArray:merchantsListLocal];
    
    
    
    
    if (merchantList.count != 0)
    {
        Merchant *aMerchant = [merchantList objectAtIndex:0];
        NSLog(@"aMerchant address is %@",aMerchant.mMailAddress1);
        merchantAddressLabel.text = aMerchant.mMailAddress1;
        addressDetailLabel.text = aMerchant.mMailAddress1;
        
        
        [lazyScrollView reloadData];
        [lazyScrollView setPage:0 transition:FORWARD animated:NO];
    }
    
    
//    merchantAddString = [merchantList objectAtIndex:0];
    
    [addressTableView reloadData];
}


- (void)parsingMerchantListXMLFailed {
    
}

#pragma mark- AlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.cancelButtonIndex == buttonIndex)
    {
        if (alertView.tag == 21) {
            // Add to fav case
            BOOL success = YES;//[appDelegate addProductToFavorites:currentProduct];
            
            [self updateFavoriteStatus];
            if(success)
            {
                NSLog(@"product added");
            }
            
            //        if (success && delegate && [delegate respondsToSelector:@selector(productAddedToFavorites:)]) {
            //            [delegate productAddedToFavorites:currentProduct];
            //        }
            
        }
        else if (alertView.tag == 22) {
            // Remove from fav case
            BOOL success = YES;//[appDelegate removeProductFromfavorites:currentProduct];
            
            [self updateFavoriteStatus];
            
            //        if (success && delegate && [delegate respondsToSelector:@selector(productremovedFromFavorites:)]) {
            //            [delegate productremovedFromFavorites:currentProduct];
            //        }
            
        }
        
    }
    else if (alertView.tag == 111) {
        NSString *phoneURLString = callNumber;
        NSString *newString = [ phoneURLString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSURL *phoneURL = [NSURL URLWithString:newString];
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
    else if(alertView.tag == 222)
    {
        if(buttonIndex == 1)
        {
            
            NSString *lattitude = @"0.00";   // [NSString stringWithFormat:@"%f",self.userLocation.latitude];
            NSString *longitude = @"0.00";// [NSString stringWithFormat:@"%f",self.userLocation.longitude];
            // NSLog(@"%@",lattitude);
            // NSLog(@"%@",longitude);
            
            NSString *urlString = [NSString stringWithFormat:@"%@redeemed.php",URL_Prefix];
            NSURL *url = [NSURL URLWithString:urlString];
            NSLog(@"url is:==> %@",url);
            
            
            redeemRequest = [[ASIFormDataRequest alloc] initWithURL:url];
            [redeemRequest setPostValue:@"sasi" forKey:@"user_id"];
            [redeemRequest setPostValue:currentProduct.productId forKey:@"pid"];
            [redeemRequest setPostValue:@"24" forKey:@"cid"];
            [redeemRequest setPostValue:lattitude forKey:@"lat"];
            [redeemRequest setPostValue:longitude forKey:@"lon"];
            
            [redeemRequest setDelegate:self];
            [redeemRequest startAsynchronous];
//            [self.redeemActivityIndicator startAnimating];
            
        }
        
    }

    
}

- (void) updateFavoriteStatus {
//    if ([appDelegate productExistsInFavorites:currentProduct]) {
//        //[favButton setTitle:@"Remove favorite" forState:UIControlStateNormal];
//        [favBtnOutlet setImage:[UIImage imageNamed:@"heart_full.png"] forState:UIControlStateNormal];
//    }
//    else {
//        //[favButton setTitle:@"Add to favorite" forState:UIControlStateNormal];
//        [favBtnOutlet setImage:[UIImage imageNamed:@"heart_empty.png"] forState:UIControlStateNormal];
//    }
}



#pragma mark- TableView DataSource and Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"Merchant List Count %lu",(unsigned long)[merchantList count]);
    return [merchantList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CellIdentifier";
    
    AddressCell *cell = (AddressCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        
        [[NSBundle bundleForClass:[self class]] loadNibNamed:@"AddressCell" owner:self options:nil];
        cell = self.cCell;
        
    }
    
    
    Merchant *aMerchant = [merchantList objectAtIndex:indexPath.row];
    
    
    NSString * str = aMerchant.mMailAddress1 ;
    
//    NSLog(@"mMailAddress1 is %@",str);
//    NSLog(@"mMailSuburb is %@",aMerchant.mMailSuburb);
//    NSLog(@"mState is %@",aMerchant.mState);
//    NSLog(@"mPhone is %@",aMerchant.mPostCode);
    
    if( aMerchant.mMailSuburb != NULL)
        str = aMerchant.mMailSuburb;
    if (aMerchant.mState != NULL)
    {
        if (str == NULL)
            str = aMerchant.mState;
        else
            str = [str stringByAppendingString:[self appendStringWithNewline:aMerchant.mState]];
        
    }
    
    if (aMerchant.mPostCode != NULL)
        str = [str stringByAppendingString:[self appendStringWithNewline:aMerchant.mPostCode]];
    
    cell.lblAddress.text = str;
    
    cell.mapButton.tag = indexPath.row +100;
    [cell.mapButton addTarget:self action:@selector(mapButtonpressed:)forControlEvents:UIControlEventTouchUpInside];
    [cell.mapButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"cm.png"] inBundle:bundle compatibleWithTraitCollection:nil ] forState:UIControlStateNormal];
    //[cell.mapButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"cm.png",bundle]] forState:UIControlStateNormal];
    cell.mapButton.userInteractionEnabled = YES;
    
    
    //if Address not found
    Location.latitude = aMerchant.coordinate.latitude;
    Location.longitude = aMerchant.coordinate.longitude;
    
    if (([aMerchant.mMailAddress1 length] == 0 && [aMerchant.mState length] == 0) || (Location.latitude == 0.000000)|| (Location.longitude == 0.000000)) {
        
        cell.lblAddress.text = @"No Address Available";
        cell.mapLabel.hidden = YES;
        cell.mapButton.hidden = YES;
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

-(IBAction)mapButtonpressed:(id)sender
{
    NSLog(@"=====MAPBUTTON PRESSED=====tag is %ld ",(long)[sender tag]);
    Merchant *aMerchant = [merchantList objectAtIndex:[sender tag] - 100];
    Location.latitude = aMerchant.coordinate.latitude;
    Location.longitude = aMerchant.coordinate.longitude;
    NSLog(@"latitude and longitude is.. %f,%f",Location.latitude,Location.longitude);
    
    
    
    NSURL *addressUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com?q=%f,%f",Location.latitude, Location.longitude, nil]];
    // NSLog(@"addressUrl  is ....%@",addressUrl);
    
    [[UIApplication sharedApplication] openURL:addressUrl];
}

-(NSString *)appendStringWithNewline:(NSString *)str
{
    NSLog(@"appendString is %@",str);
    NSString *appendStr = [NSString stringWithFormat:@"\n%@",str];
    
    NSLog(@"after appendString is %@",appendStr);
    return appendStr;
}

@end
