//
//  SearchViewController.m
//  GrabItNow
//
//  Created by MyRewards on 11/25/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//
#define URL_Prefix @"http://www.myrewards.com.au/app/webroot/newapp/"
#define Default_keyboard_height_iPhone 216.0

#import "SearchViewController.h"
#import <SearchFrameWork/ASIHTTPRequest.h>
#import <SearchFrameWork/ASIFormDataRequest.h>

#import <QuartzCore/QuartzCore.h>
#import "cat.h"
#import "ProductListViewController.h"


@interface SearchViewController ()
{
    
    
    BOOL showingSettingsView;
    
    NSString *locString;
    NSString *keyString;
    
    NSMutableArray *categoryArray;
    
    ASIFormDataRequest *categoryFetchRequest;
    ASIFormDataRequest *keywordSearchrequest;
    ASIFormDataRequest *categorySearchRequest;
    
    CategoryXMLParser *categoryParserDelegate;
     ProductListParser *productsXMLParser;

    cat *category;
    

    
    int selectedCategory;
    
    int currentPageNo;
    NSString *helpText;
    BOOL isMapSelected;
    ProductListViewController *productsListController;
    NSBundle *bundle;
}

@property (nonatomic, strong) ProductListViewController *productsListController;

- (void) fetchCategoryList;
- (void) searchProductsOnKeywordBasis;
@end

@implementation SearchViewController



@synthesize tblView;
@synthesize toolBar;
@synthesize settingsView;

@synthesize helpText_webView;

@synthesize locationLabel;
@synthesize keywordLabel;
@synthesize locationEditTextField;
@synthesize keywordEditTextField;
@synthesize infoView;
@synthesize activityView;
@synthesize currentTextfield;
@synthesize mapResultButton;
@synthesize infoButton;
- (id)init {
    bundle = [NSBundle bundleForClass:[self class]];
    if ((self = [super initWithNibName:@"SearchViewController" bundle:bundle])) {
    }
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        showingSettingsView = NO;
        
        locString = @"";
        keyString = @"";
        isMapSelected = NO;
        
        
        selectedCategory = -1;
        
        currentPageNo = 0;
        
        
        helpText =@"<h3>  &nbsp &nbsp &nbsp &nbsp Searching made easy!</h3> This function allows you to search by category ,location and keywords<br><br> 1.  Select the category in the menu .<br> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp      EG: dining and fast food  <br><br>2. Enter a location such as a city, town or suburb.  <br>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp           EG: Carlton or Auckland  <br><br> 3. Enter a key word  <br> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp  EG : Pizza  <br><br> The search will look for a dining offers that are located  in Carlton and has pizza in the key words.  <br><br>If your search is unsuccessful we recommend that you broaden the search parameters.   <br><br>To broaden your  search simply select the category and then enter a location and exclude any keywords.  <br><br>All the offers that match your category and location will be displayed ";
        
    }
    return self;
}

- (void)dealloc {
    
    if (categoryFetchRequest) {
        [categoryFetchRequest cancel];
        categoryFetchRequest = nil;
    }
    
    if (categorySearchRequest) {
        [categorySearchRequest cancel];
        categorySearchRequest = nil;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    selectedCategory = -1;
    
    tblView.dataSource=self;
    tblView.delegate=self;
    categoryArray = [[NSMutableArray alloc] init];
    // ** Apply round corners to the Container
    UIView *infoLabelBgView = [infoView viewWithTag:10];
    infoLabelBgView.layer.borderColor = [UIColor colorWithWhite:1.0 alpha:1.0].CGColor;
    infoLabelBgView.layer.borderWidth = 2.0;
    infoLabelBgView.layer.cornerRadius = 4.0;
    
    settingsView.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
    settingsView.layer.shadowOpacity = 1.0;
    settingsView.layer.shadowOffset = CGSizeMake(0.0, -3.0);
    settingsView.layer.shadowRadius = 3.0;
    
    
    helpText = [NSString stringWithFormat:@"<html> \n"
                "<head> \n"
                "<style type=\"text/css\"> \n"
                "body {font-family: \"%@\"; font-size: %@; font-color: \"%@\"}\n"
                "</style> \n"
                "</head> \n"
                "<body>%@</body> \n"
                "</html>", @"Helvetica", [NSNumber numberWithInt:15],@"#FF00000", helpText];
    
    [self.helpText_webView setOpaque:NO];
    // [self.helpText_webView loadHTMLString:helpText baseURL:nil];
    [self.helpText_webView loadHTMLString:[NSString stringWithFormat:@"<html><body p style='color:white' text=\"#FFFFFF\" face=\"Bookman Old Style, Book Antiqua, Garamond\" size=\"5\">%@</body></html>", helpText] baseURL: nil];
    
    [self fetchCategoryList];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.title=@"Search";
    [tblView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillLayoutSubviews {
    /*
     CGRect toolbarFrame = self.toolBar.frame;
     toolbarFrame.origin.y = self.view.frame.size.height - toolbarFrame.size.height;
     self.toolBar.frame = toolbarFrame;
     */
    
    CGRect settingsViewFrame = settingsView.frame;
    settingsViewFrame.origin.y = self.view.frame.size.height - settingsViewFrame.size.height;
    settingsView.frame = settingsViewFrame;
    
    CGRect tableFrame = tblView.frame;
    tableFrame.size.height = self.view.frame.size.height - settingsViewFrame.size.height;
    tblView.frame = tableFrame;
    
    CGRect infoButtonFrame = infoButton.frame;
    infoButtonFrame.origin.y = self.view.frame.size.height - 132.0;
    infoButton.frame = infoButtonFrame;
    
}

- (void) showActivityView {
    //activityView.frame = self.view.bounds;
    UIView *loadingLabelView = [activityView viewWithTag:10];
    loadingLabelView.center = activityView.center;
    [self.view addSubview:activityView];
}

- (void) dismissActivityView {
    [activityView removeFromSuperview];
}

- (void) fetchCategoryList {
    
   // NSLog(@"... fetchCategoryList ..., cid=%@ && country=%@",appDelegate.sessionUser.client_id,appDelegate.sessionUser.country);
    [self showActivityView];
    
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@get_cat.php?cid=%@&country=%@",URL_Prefix,[[NSUserDefaults standardUserDefaults] stringForKey:@"client_id"],[[NSUserDefaults standardUserDefaults] stringForKey:@"country"]]stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    NSLog(@"url is:%@",url);
    categoryFetchRequest = [ASIFormDataRequest requestWithURL:url];
    [categoryFetchRequest setDelegate:self];
    [categoryFetchRequest startAsynchronous];
    
}

- (void) searchProductsOnKeywordBasis {
    
    [self showActivityView];
    
    cat *cat;
    NSString *categoryID = @"-1";
    if(selectedCategory != -1)
    {
        cat = [categoryArray objectAtIndex:selectedCategory];
        categoryID = cat.catId;
    }
    
    NSString *urlString;
    if(!isMapSelected)
        urlString = [NSString stringWithFormat:@"%@search.php?",URL_Prefix];
    else
        urlString = [NSString stringWithFormat:@"%@search_with_map.php?",URL_Prefix];
    
    if(selectedCategory != -1)
    {
        urlString = [NSString stringWithFormat:@"%@cat_id=%@",urlString,categoryID];
        
    }
    if([locationEditTextField.text length]>0)
    {
        
        if(selectedCategory != -1)
        {
            
            urlString = [NSString stringWithFormat:@"%@&",urlString];
            
        }
        urlString = [NSString stringWithFormat:@"%@p=%@",urlString,locationEditTextField.text];
    }
    if([keywordEditTextField.text length]>0)
    {
        
        if(selectedCategory != -1 || ([locationEditTextField.text length]!= 0))
        {
            
            urlString = [NSString stringWithFormat:@"%@&",urlString];
            
        }
        
        urlString = [NSString stringWithFormat:@"%@q=%@",urlString,keywordEditTextField.text];
    }
    
    if(!isMapSelected)
    {
        urlString = [NSString stringWithFormat:@"%@&cid=%@&country=%@&start=0&limit=30",urlString,[[NSUserDefaults standardUserDefaults] stringForKey:@"client_id"],[[NSUserDefaults standardUserDefaults] stringForKey:@"country"]];
        NSLog(@"Search URL: %@",urlString);
        
        
        
        
        
    }
    else{
        urlString = [NSString stringWithFormat:@"%@&cid=%@&country=%@",urlString,[[NSUserDefaults standardUserDefaults] stringForKey:@"client_id"],[[NSUserDefaults standardUserDefaults] stringForKey:@"country"]];
        NSLog(@"Search URL: %@",urlString);
        
//         nearByViewController = [[NearByViewController alloc] initWithNibName:@"NearByViewController" bundle:nil url:urlString];
//         [self.navigationController pushViewController:nearByViewController animated:YES];
        
        
        //  [self performSelector:@selector(dismissActivityView) withObject:nil afterDelay:0.2];
        
    }
    //to trim Spaces in Url
    
    NSString *searchURL = [urlString stringByTrimmingCharactersInSet:
                           [NSCharacterSet whitespaceCharacterSet]];
    
    NSLog(@"URL TRIMMED: %@",searchURL);
    NSURL *url = [NSURL URLWithString:[searchURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    categorySearchRequest = [ASIFormDataRequest requestWithURL:url];
    [categorySearchRequest setDelegate:self];
    [categorySearchRequest startAsynchronous];
}

- (IBAction)searchButtonTapped:(id)sender {
    
    NSLog(@"selectedCategory:::%d",selectedCategory);
    
    if ((selectedCategory == -1 && locationEditTextField.text.length == 0) && keywordEditTextField.text.length == 0)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select a search criteria" message:@"Please select a category or enter a keyword or a location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        
        return;
    }
    
    if (selectedCategory == -1 /* && keywordEditTextField.text.length != 0  */)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select a search criteria" message:@"Please select a category " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
        
    }
    
    [self searchProductsOnKeywordBasis];
    
    [currentTextfield resignFirstResponder];
    
    
}

- (IBAction)settingsButtonTapped:(id)sender {
    
    if (showingSettingsView) {
        // Remove the menuView
        showingSettingsView = NO;
        
        [self settingsViewClosed];
        
        [UIView animateWithDuration:0.5 animations:^{
            CGRect settingsViewFrame = settingsView.frame;
            settingsViewFrame.origin.y = toolBar.frame.origin.y;
            settingsView.frame = settingsViewFrame;
        } completion:^(BOOL finished) {
            [settingsView removeFromSuperview];
        }];
        
    }
    else {
        
        if ([settingsView superview]) {
            // This means menu closing/opening animation is happening.
            // Do nothing here
            return;
        }
        
        CGRect settingsViewFrame = settingsView.frame;
        settingsViewFrame.origin.y = toolBar.frame.origin.y;
        settingsView.frame = settingsViewFrame;
        
        [self.view insertSubview:settingsView belowSubview:toolBar];
        
        
        settingsViewFrame.origin.y = toolBar.frame.origin.y - settingsViewFrame.size.height;
        
        [UIView animateWithDuration:0.5 animations:^{
            settingsView.frame = settingsViewFrame;
        } completion:^(BOOL finished) {
            showingSettingsView = YES;
        }];
    }
}


- (void) settingsViewClosed {
    
    // Finally update labels
    locationLabel.text = locString;
    keywordLabel.text = keyString;
    
}

- (void)showInfoView {
    
    infoView.frame = self.view.bounds;
    [self.view addSubview:infoView];
    [self.view bringSubviewToFront:infoButton];
    
}




- (void) dismissInfoView {
    [self.infoView removeFromSuperview];
}


- (IBAction)infoButtonTapped:(id)sender {
    
    UIButton *infoButtonLocal = (UIButton *)sender;
    infoButtonLocal.selected = !infoButtonLocal.selected;
    
    
    if (infoButtonLocal.selected) {
        [self showInfoView];
    }
    else {
        [self dismissInfoView];
    }
    [currentTextfield resignFirstResponder];
    
}


- (IBAction)resultWithMapTapped:(id)sender
{
    if(isMapSelected){
        [self.mapResultButton setImage:[UIImage imageNamed:@"checkbox.png"] forState:UIControlStateNormal];
        isMapSelected = NO;
    }
    else{
        
        [self.mapResultButton setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
        isMapSelected = YES;
    }
    
    
}



- (IBAction)resetButtonTapped:(id)sender {
    selectedCategory = -1;
    [tblView reloadData];
    
    locationEditTextField.text = @"";
    keywordEditTextField.text = @"";
    
    if(isMapSelected){
        [self.mapResultButton setImage:[UIImage imageNamed:@"checkbox.png"] forState:UIControlStateNormal];
        isMapSelected = NO;
    }
    
}

#pragma mark - UItableview datasource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [categoryArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cat *catObj = [categoryArray objectAtIndex:indexPath.row];
    
    cell.textLabel.text = catObj.catName;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:20];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    //cell.contentView.backgroundColor = [UIColor blackColor];
    //  cell.textLabel.font = [UIFont ]
    
    [UIImage imageNamed:@"homeAndLifeStyle_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
    
    UIImageView *radioButton = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 21, 21)];
    radioButton.image = (selectedCategory == indexPath.row) ? [UIImage imageNamed:@"radio-button_on.png" inBundle:bundle compatibleWithTraitCollection:nil ] : [UIImage imageNamed:@"radio-button_off.png" inBundle:bundle compatibleWithTraitCollection:nil ] ;
    cell.accessoryView = radioButton;
    
    if ([catObj.catName isEqualToString:@"Automotive"])
    {
        cell.imageView.image = [UIImage imageNamed:@"automobile_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
    }
    else if ([catObj.catName isEqualToString:@"Body & Soul"])
    {
        cell.imageView.image =  [UIImage imageNamed:@"bodyAndSoul_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
    }
    else if ([catObj.catName isEqualToString:@"Dining & Fast Food"])
    {
        cell.imageView.image = [UIImage imageNamed:@"diningAndFast_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
    }
    else if ([catObj.catName isEqualToString:@"Financial Services"])
    {
        cell.imageView.image = [UIImage imageNamed:@"financialServices_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
    }
    else if  ([catObj.catName isEqualToString:@"Golf Courses"])
    {
        cell.imageView.image =  [UIImage imageNamed:@"golfCourse_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
    }
    else if  ([catObj.catName isEqualToString:@"Handicaps"]||[catObj.catName isEqualToString:@"Golf Handicaps"])
    {
        cell.imageView.image = [UIImage imageNamed:@"golfHandicaps_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
    }
    else if  ([catObj.catName isEqualToString:@"Health & Beauty"])
    {
        cell.imageView.image = [UIImage imageNamed:@"healthAndBeauty_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
    }
    else if  ([catObj.catName isEqualToString:@"Home & Lifestyle"])
    {
        cell.imageView.image = [UIImage imageNamed:@"homeAndLifeStyle_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];    }
    
    else if  ([catObj.catName isEqualToString:@"Insurance"]||[catObj.catName isEqualToString:@"Golf Insurance"])
    {
        cell.imageView.image = [UIImage imageNamed:@"golfInsurance_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
    }
    else if ([catObj.catName isEqualToString:@"Leisure & Entertainment"])
    {
        cell.imageView.image = [UIImage imageNamed:@"leisureAndEntertainment_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
    }
    else if ([catObj.catName isEqualToString:@"Shopping & Vouchers"])
    {
        cell.imageView.image = [UIImage imageNamed:@"Shopping_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
    }
    else if ([catObj.catName isEqualToString:@"Travel & Accommodation"])
    {
        cell.imageView.image = [UIImage imageNamed:@"travel_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
    }
    else
    {
        cell.imageView.image = [UIImage imageNamed:@"empty_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
    }
    
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int prevSelection = selectedCategory;
    selectedCategory = indexPath.row;
    
    if (prevSelection != -1) {
        [tblView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:prevSelection inSection:0], nil] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [tblView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    
    
    [UIView animateWithDuration:0.28 animations:^{
        CGRect settingsViewFrame = settingsView.frame;
        settingsViewFrame.origin.y = self.view.frame.size.height - settingsViewFrame.size.height;
        settingsView.frame = settingsViewFrame;
        CGRect infoframe=infoButton.frame;
        infoframe.origin.y=self.view.frame.size.height - settingsViewFrame.size.height+15;
        infoButton.frame=infoframe;
    } completion:^(BOOL finished) {
        
    }];
    
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 25) ? NO : YES;
}



-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    self.currentTextfield = textField;
    
    
    
    [UIView animateWithDuration:0.28 animations:^{
        CGRect settingsFrame = settingsView.frame;
        settingsFrame.origin.y = self.view.frame.size.height - Default_keyboard_height_iPhone - settingsFrame.size.height;
        settingsView.frame = settingsFrame;
        CGRect infoframe=infoButton.frame;
        infoframe.origin.y=self.view.frame.size.height - Default_keyboard_height_iPhone - settingsFrame.size.height+15;
        infoButton.frame=infoframe;
    }  completion:^(BOOL finished) {
        
    }];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == locationEditTextField) {
        locString = locationEditTextField.text;
    }
    else if (textField == keywordEditTextField) {
        keyString = keywordEditTextField.text;
    }
    //   self.currentTextfield = textField;
    [textField resignFirstResponder];
}


#pragma mark -- ASIHttpRequest Delegate methods

- (void)requestFinished:(ASIHTTPRequest *)request {
    
    if (request == categoryFetchRequest) {
        categoryFetchRequest = nil;
        NSLog(@"Search response: %@",[request responseString]);
        NSXMLParser *categoryParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        categoryParserDelegate = [[CategoryXMLParser alloc] init];
        categoryParserDelegate.delegate = self;
        categoryParser.delegate = categoryParserDelegate;
        [categoryParser parse];
    }
    else if (request == categorySearchRequest) {
        
        //categorySearchRequest = nil;
        [currentTextfield resignFirstResponder];
        NSLog(@"Search response: %@",[request responseString]);
        
        if([[request responseString] length] == 0 ||[request responseString] == NULL )
        {
            UIAlertView *responseAlert = [[UIAlertView alloc]initWithTitle:@"GRAB IT NOW" message:@"No Results Found" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [responseAlert show];
            
        }
        else
        {
            NSXMLParser *productsParser = [[NSXMLParser alloc] initWithData:[request responseData]];
            productsXMLParser = [[ProductListParser alloc] init];
            productsXMLParser.delegate = self;
            productsParser.delegate = productsXMLParser;
            [productsParser parse];
        }
        
    }
    
    [self dismissActivityView];
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    
    [currentTextfield resignFirstResponder];
    NSLog(@"request failed %@",[request responseString]);
    request = nil;
    
    
    [self dismissActivityView];
    
}

#pragma mark -- Category Parser Delegate methods

- (void)parsingCategoriesFinished:(NSArray *)categoryList {
    NSLog(@" parsingCategoriesFinished is %lu",(unsigned long)categoryList.count);
    
    //NSMutableArray *myArray = [[NSMutableArray alloc]init];
    
    for (category in categoryList) {
        NSLog(@"%@",category.catName);
        [categoryArray addObject:category];
      //  [myArray addObject:category];
        
    }
    
     //for (category in categoryArray)
       //  NSLog(@"----%@",category.catName);
    
    [self.tblView reloadData];
    
}

- (void)categoryXMLparsingFailed {
    
}

#pragma mark -- Product Parser Delegate methods

- (void)parsingProductListFinished:(NSArray *)prodcutsList {
    
    NSLog(@"parsingProductListFinished count is %lu ",(unsigned long)prodcutsList.count);
    
    
    if(!isMapSelected)
    {
        
        // Here pass control to Products List view controller
        ProductListViewController *viewController = [[ProductListViewController alloc] initWithNibName:@"ProductListViewController" bundle:bundle productListType:ProductListTypeSearch];
        //viewController.productsList = [[NSMutableArray alloc] initWithArray:prodcutsList];
        productsListController = viewController;
        //[self presentViewController:viewController animated:NO completion:nil];
        
        [[self navigationController] pushViewController:viewController animated:YES];
        [productsListController setInitialProductsList:prodcutsList];
        
        cat *cat = [categoryArray objectAtIndex:selectedCategory];
        [productsListController setSearchCategoryID:cat.catId keyword:keyString location:locString];
    }
    
    else{
        
//        nearByViewController = [[NearByViewController alloc] initWithNibName:@"NearByViewController" bundle:nil proArray:prodcutsList];
//        
//        
//        NSString *str = [NSString stringWithFormat:@"%d",selectedCategory+1];
//        [nearByViewController setSearchCategoryID:str];
//        
//        
//        [self.navigationController pushViewController:nearByViewController animated:YES];
        
        
    }
    
}

- (void)parsingProductListXMLFailed {
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (alertView.cancelButtonIndex == buttonIndex)
        
        NSLog(@"Won....");
    
    else
    {
        
        [self dismissActivityView];
        
    }
}

@end


