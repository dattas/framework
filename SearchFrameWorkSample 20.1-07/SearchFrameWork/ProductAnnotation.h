//
//  ProductAnnotation.h
//  SearchFrameWorkSample
//
//  Created by vairat on 17/07/15.
//  Copyright (c) 2015 vaisoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "Product.h"


@interface ProductAnnotation : NSObject<MKAnnotation>

@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;

// Title and subtitle for use by selection UI.
@property (nonatomic, readwrite, copy) NSString *title;
@property (nonatomic, readwrite, copy) NSString *subtitle;
@property (nonatomic,strong) Product *prod;
@property (nonatomic, readwrite) NSInteger prodAnnTag;
@property (nonatomic, readwrite, copy) NSString *pinColor;
@end
