//
//  CategoryXMLParser.m
//  GrabItNow
//
//  Created by MyRewards on 12/22/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import "CategoryXMLParser.h"

@interface CategoryXMLParser()
{
    NSMutableArray *categoryList;
    NSMutableString *charString;
    cat *currCategory;
}

@property (nonatomic, strong) NSMutableArray *categoryList;

@end

@implementation CategoryXMLParser

@synthesize delegate;
@synthesize categoryList;


- (void)parserDidStartDocument:(NSXMLParser *)parser {
    categoryList = [[NSMutableArray alloc] init];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {

    charString = nil;
   
    if ([elementName isEqualToString:@"category"]) {
        
        if (currCategory) {
            currCategory = nil;
        }
        
        currCategory = [[cat alloc] init];
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {

    if (!charString) {
        charString = [[NSMutableString alloc] initWithString:string];
    }
    else {
        [charString appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    NSString *finalString = [charString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([elementName isEqualToString:@"category"]) {
        [categoryList addObject:currCategory];
        currCategory = nil;
    }
    else if ([elementName isEqualToString:@"id"]) {
        currCategory.catId = finalString;
    }
    else if ([elementName isEqualToString:@"name"]) {
        currCategory.catName = finalString;
    }
    else if ([elementName isEqualToString:@"description"]) {
        currCategory.catDescription = finalString;
    }
    else if ([elementName isEqualToString:@"root"]) {
         NSLog(@"catogery requestFinished");
        [delegate parsingCategoriesFinished:categoryList];
    }
}


- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    NSLog(@"catogery requestFailed");
    [delegate categoryXMLparsingFailed];
}

@end
