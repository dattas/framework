//
//  SearchViewController.h
//  SearchFrameWorkSample
//
//  Created by vairat on 07/07/15.
//  Copyright (c) 2015 vaisoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SearchFrameWork/ASIHTTPRequest.h>
#import <searchFrameWork/CategoryXMLParser.h>
#import "ProductListParser.h"
@interface SearchViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, ASIHTTPRequestDelegate, CategoryXMLParserDelegate,ProductListXMLParserDelegate>
@property (nonatomic, strong) IBOutlet UILabel *locationLabel;
@property (nonatomic, strong) IBOutlet UILabel *keywordLabel;

@property (nonatomic, strong) IBOutlet UITextField *locationEditTextField;
@property (nonatomic, strong) IBOutlet UITextField *keywordEditTextField;

@property (nonatomic, strong) IBOutlet UITableView *tblView;
@property (nonatomic, strong) IBOutlet UIView *toolBar;
@property (nonatomic, strong) IBOutlet UIView *settingsView;
@property (nonatomic, strong) IBOutlet UIView *infoView;
@property (nonatomic, strong) IBOutlet UIView *activityView;

@property (nonatomic, strong) IBOutlet UIButton *infoButton;
@property (nonatomic, strong) IBOutlet UIWebView *helpText_webView;

@property (nonatomic, strong) UITextField *currentTextfield;
@property (nonatomic, strong) IBOutlet UIButton *mapResultButton;


- (IBAction)infoButtonTapped:(id)sender;


- (IBAction)resultWithMapTapped:(id)sender;
- (IBAction)settingsButtonTapped:(id)sender;
- (IBAction)searchButtonTapped:(id)sender;
- (IBAction)resetButtonTapped:(id)sender;

@end
