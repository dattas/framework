//
//  cat.m
//  Coredata
//
//  Created by vairat on 07/07/15.
//  Copyright (c) 2015 Vairat. All rights reserved.
//

#import "cat.h"

@implementation cat
@synthesize catId;
@synthesize catName;
@synthesize catDescription;
@synthesize catImgURLString;
@end
