//
//  MyCardViewController.m
//  GrabItNow
//
//  Created by Monish Kumar on 06/01/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "MyCardViewController.h"
#import "ASIFormDataRequest.h"
#import "AppDelegate.h"
#import <SearchFrameWork.h>

@interface MyCardViewController ()
{
    ASIFormDataRequest *imageRequest;
    AppDelegate *appDelegate;
}
- (void) myCardsDisplay;
@end
//MyCardViewController

@implementation MyCardViewController
@synthesize myCardImageView, userNameLabel, clientIdLabel,userNameLabel1;
- (id)init {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    if ((self = [super initWithNibName:@"MyCardViewController" bundle:bundle])) {
    }
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil 
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
//    [appDelegate.homeViewController hideBackButon:YES];
//    [appDelegate.homeViewController setHeaderTitle:@"My Membership Card"];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self myCardsDisplay];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}


- (void) myCardsDisplay {
    
        
    
    
    // [[NSUserDefaults standardUserDefaults] stringForKey:@"client_id"]
    //[[NSUserDefaults standardUserDefaults] stringForKey:@"country"]

    
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@.%@",MyCard_URL_Prefix,[[NSUserDefaults standardUserDefaults] stringForKey:@"client_id"],[[NSUserDefaults standardUserDefaults] stringForKey:@"card_ext"]];
    
    NSLog(@"Image URL: %@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
  
    NSData *data = [[NSData alloc] initWithContentsOfURL:url];
    UIImage *img = [[UIImage alloc] initWithData:data];
    myCardImageView.image = img;
    
    NSString *userNameStr = [NSString stringWithFormat:@"Name: %@ %@",[[NSUserDefaults standardUserDefaults] stringForKey:@"first_name"], [[NSUserDefaults standardUserDefaults] stringForKey:@"last_name"]];
    userNameLabel.text = userNameStr;
    userNameLabel.transform = CGAffineTransformMakeRotation (-3.14/2);
    
    clientIdLabel.text = [NSString stringWithFormat:@"Client: %@", [[NSUserDefaults standardUserDefaults] stringForKey:@"client_name"]];
    clientIdLabel.transform = CGAffineTransformMakeRotation (-3.14/2);
    
    userNameLabel1.text = [NSString stringWithFormat:@"Membership: %@", [[NSUserDefaults standardUserDefaults] stringForKey:@"first_name"]];
    userNameLabel1.transform = CGAffineTransformMakeRotation (-3.14/2);

    // Update label frames
    CGRect clientIdLabelFrame = clientIdLabel.frame;
    CGRect mrlogoBaseViewFrame = self.mrlogoBaseView.frame;
    
    if([ [ UIScreen mainScreen ] bounds ].size.height == 568.0){
      clientIdLabelFrame.origin.y = 120.0;
        clientIdLabelFrame.origin.x = myCardImageView.frame.origin.x - clientIdLabelFrame.size.width;
        mrlogoBaseViewFrame.origin.y = myCardImageView.frame.origin.y + myCardImageView.bounds.size.height +77;}
    else{
       clientIdLabelFrame.origin.y = 70.0;
        clientIdLabelFrame.origin.x = (myCardImageView.frame.origin.x - clientIdLabelFrame.size.width)+15;
        mrlogoBaseViewFrame.origin.y = myCardImageView.frame.origin.y + myCardImageView.bounds.size.height -12;}
    clientIdLabel.frame = clientIdLabelFrame;
    self.mrlogoBaseView.frame = mrlogoBaseViewFrame;
    
    CGRect userNameLabelFrame = userNameLabel.frame;
    if([ [ UIScreen mainScreen ] bounds ].size.height == 568.0)
    userNameLabelFrame.origin.x = myCardImageView.frame.origin.x + myCardImageView.frame.size.width;
    else
     userNameLabelFrame.origin.x = (myCardImageView.frame.origin.x + myCardImageView.frame.size.width)-10;
    userNameLabelFrame.origin.y = 120;
    userNameLabel.frame = userNameLabelFrame;
    
    CGRect userNameLabel1Frame = userNameLabel1.frame;
    userNameLabel1Frame.origin.x = userNameLabelFrame.origin.x + userNameLabel.frame.size.width;
    userNameLabel1Frame.origin.y = 120;//userNameLabelFrame.origin.y;
    userNameLabel1.frame = userNameLabel1Frame;
}


#pragma mark -- ASIHttpRequest Delegate methods

- (void)requestFinished:(ASIHTTPRequest *)request {
    
   
          
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    UIAlertView *failedAlert=[[UIAlertView alloc]initWithTitle:@"Failed" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [failedAlert show];
    NSLog(@"Image Data -- requestFailed:");
    
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
