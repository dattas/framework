//
//  sampleViewController.h
//  SearchFrameWorkSample
//
//  Created by vairat on 07/07/15.
//  Copyright (c) 2015 vaisoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import <SearchFrameWork/UserDataXMLParser.h>

@interface sampleViewController : UIViewController<ASIHTTPRequestDelegate,UserDataXMLParser>
{
    NSString *userID;
    
}
@property (nonatomic, strong) NSString *userID;
@end
