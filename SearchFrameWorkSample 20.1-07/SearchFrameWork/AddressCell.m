//
//  AddressCell.m
//  PDKTStickySectionHeadersCollectionViewLayoutDemo
//
//  Created by vairat on 16/03/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "AddressCell.h"

@implementation AddressCell
@synthesize lblAddress, mapButton;

- (void)awakeFromNib {
    // Initialization code
    [mapButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@/cm.png",[NSBundle bundleForClass:[self class]]]] forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
