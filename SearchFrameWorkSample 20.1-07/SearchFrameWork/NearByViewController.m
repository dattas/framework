//
//  NearByViewController.m
//  GrabItNow
//
//  Created by Monish Kumar on 06/01/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "NearByViewController.h"
#import "ProductDetailViewController.h"
#import "ASIFormDataRequest.h"
#import "AppDelegate.h"
#import "ASIHTTPRequest.h"
#import "ProductDataParser.h"
#import "ProductListParser.h"
#import "ProductAnnotation.h"
#import "ProductListViewController.h"
//#import "ZSPinAnnotation.h"
#import <MapKit/MapKit.h>

@interface NearByViewController ()
{
    ASIFormDataRequest *categoryNearByRequest;
    ASIFormDataRequest * productFetchRequest;
    
    AppDelegate *appDelegate;
    ProductListParser *productsXMLParser;
    ProductDataParser *productDataXMLParser;
    //  BOOL isSearchCalled;
    NSMutableArray *productsList;
    NSString *category;
    NSArray *products_Array;
    NSMutableArray *repAnnonatastions;
    NSBundle *bundle;
}
@property(nonatomic,strong) NSMutableArray *productsList;
@property(nonatomic,strong)NSMutableArray *repAnnonatastions;
-(void)getCurrentLocation;
- (void) fetchProductWithProductID:(NSString *) productId;
- (void) showActivityView;
- (void) dismissActivityView;
- (void)mutateCoordinatesOfClashingAnnotations:(NSArray *)annotations;
@end

@implementation NearByViewController
@synthesize mkView;
@synthesize locationManager;
@synthesize currentLocation;
@synthesize productsList;
@synthesize productController;
@synthesize prodDetail;
@synthesize activityView;
@synthesize repAnnonatastions;
- (id)init {
    bundle = [NSBundle bundleForClass:[self class]];
    if ((self = [super initWithNibName:@"NearByViewController" bundle:bundle])) {
    }
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil proArray:(NSArray *)proList;
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        products_Array = [[NSArray alloc]initWithArray:proList];
        
    }
    return self;
}

- (void)viewDidLoad
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //  isSearchCalled = NO;
    [mkView setMapType:MKMapTypeStandard];
    [mkView setZoomEnabled:YES];
    [mkView setScrollEnabled:YES];
    [self getCurrentLocation];
    
    [mkView setDelegate:self];
    
    
    repAnnonatastions=[[NSMutableArray alloc]init];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void) viewWillAppear:(BOOL)animated
{
    NSLog(@"===viewWillAppear====");
    [super viewWillAppear:animated];
//    if ([products_Array count] == 0)
//       // [appDelegate.homeViewController hideBackButon:YES];
//    else
//      //  [appDelegate.homeViewController hideBackButon:NO];
    
    
 //   [appDelegate.homeViewController setHeaderTitle:@"What's Around Me"];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    if (![CLLocationManager locationServicesEnabled]) {
        
        NSLog(@"===location Services not Enabled ====");
        UIAlertView *locatioAlert = [[UIAlertView alloc] initWithTitle:@"ALERT"
                                                               message:@"Your location sevices are not enabled."
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [locatioAlert show];
    }
    
    
    
}

- (void)updateCurrentLocation {
    
    if ([CLLocationManager locationServicesEnabled]) {
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager performSelector:@selector(requestWhenInUseAuthorization)];
        }
        // .. do something here ..//
    }
    
    [self.locationManager startUpdatingLocation];
}


-(void)getCurrentLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    //  CLLocation *location = [locationManager location];
    // Configure the new event with information from the location
    
    
    //Vinod Added
    [self updateCurrentLocation];
    
    // return coordinate;
}

- (void)viewDidUnload
{
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) setSearchCategoryID:(NSString *) catId
{
    NSLog(@"%@",catId);
    category = catId;
}

- (void) searchProductsOnKeywordBasis {
    
  //  [[NSUserDefaults standardUserDefaults] stringForKey:@"client_id"]
    // Make server call for more products.
    NSLog(@"\n Lat = %f, long = %f in searchProductsOnKeywordBasis",self.currentLocation.latitude,self.currentLocation.longitude);
    NSString *urlString = [NSString stringWithFormat:@"%@get_products_by_loc.php?lat=%f&lng=%f&cid=%@&b=%f&c=%@",URL_Prefix,self.currentLocation.latitude,self.currentLocation.longitude,[[NSUserDefaults standardUserDefaults] stringForKey:@"client_id"],0.1,[[NSUserDefaults standardUserDefaults] stringForKey:@"country"]];
    
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"url is %@",url);
    
    categoryNearByRequest = [ASIFormDataRequest requestWithURL:url];
    [categoryNearByRequest setDelegate:self];
    [categoryNearByRequest startAsynchronous];
    
    
}

- (void) addNewAnnotations
{
    // NSLog(@"==========addNewAnnotations===========");
    NSLog(@"PRODUCTS COUNT: %d",[productsList count]);
    //int i=0;
    for(Product *prod in productsList)
    {
        // NSLog(@"\n Product offers in addNewAnnotations Method = %@",prod.productOffer);
        ProductAnnotation *ann = [[ProductAnnotation alloc]init];
        ann.coordinate = prod.coordinate;
        ann.title = prod.productName;
        //ann.subtitle=prod.pin_type;
        ann.prod = prod;
        ann.pinColor = prod.pin_type;
        
        [mkView addAnnotation:ann];
        [repAnnonatastions addObject:ann];
        
    }
    //    NSLog(@"================================================");
    //    for (ProductAnnotation *ann  in repAnnonatastions ) {
    //
    //        NSLog(@"title::%@",ann.title);
    //        NSLog(@"pinType::%@",ann.subtitle);
    //        NSLog(@"latitude::%f",ann.coordinate.latitude);
    //        NSLog(@"longitude::%f",ann.coordinate.longitude);
    //        NSLog(@"-----------------------------------------------");
    //    }
    //    NSLog(@"================================================");
    
    [self mutateCoordinatesOfClashingAnnotations:repAnnonatastions];
    
    
}
/*{
 NSLog(@"==========AddNewAnnotations===========");
 NSLog(@"............ PODUCTS COUNT: %d",[productsList count]);
 //int i=0;
 
 if([products_Array count] == 0)
 {
 for(Product *prod in productsList)
 {
 // NSLog(@"\n Product offers in addNewAnnotations Method = %@",prod.productOffer);
 ProductAnnotation *ann = [[ProductAnnotation alloc]init];
 ann.coordinate = prod.coordinate;
 ann.title = prod.productName;
 ann.prod = prod;
 ann.pinColor = prod.pin_type;
 // ann.prodAnnTag = i++;
 [mkView addAnnotation:ann];
 [repAnnonatastions addObject:ann];
 
 [self mutateCoordinatesOfClashingAnnotations:repAnnonatastions];
 }
 }
 
 else
 {
 for(Product *prod in products_Array)
 {
 NSLog(@"\n Product offers in addNewAnnotations Method = %@",prod.productName);
 ProductAnnotation *ann = [[ProductAnnotation alloc]init];
 ann.coordinate = prod.coordinate;
 ann.title = prod.productName;
 ann.prod = prod;
 ann.pinColor = category;
 [mkView addAnnotation:ann];
 [repAnnonatastions addObject:ann];
 
 [self mutateCoordinatesOfClashingAnnotations:repAnnonatastions];
 }
 
 }
 //[mkView reloadInputViews];
 }*/


#pragma mark CoreLocation Delegate Methods.........

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"==========locationManager===========");
    CLLocationCoordinate2D coordinate = [newLocation coordinate];
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    
    
    NSLog(@"latitude : %@", latitude);
    NSLog(@"longitude : %@",longitude);
    self.currentLocation = coordinate;
    MKCoordinateRegion region = mkView.region;
    
    region.center.latitude = self.currentLocation.latitude;
    region.center.longitude = self.currentLocation.longitude;
    
    if ([products_Array count] == 0) {
        region.span.longitudeDelta = 0.05f;
        region.span.latitudeDelta = 0.0f;
    }
    else{
        region.span.longitudeDelta = 17.05f;
        region.span.latitudeDelta = 17.0f;
    }
    [mkView setRegion:region animated:YES];
    [mkView showsUserLocation];
    
    if([products_Array count] == 0)
        [self searchProductsOnKeywordBasis];
    else
        [self addNewAnnotations];
    [locationManager stopUpdatingLocation];
    
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if (error.code ==  kCLErrorDenied) {
        NSLog(@"Location manager denied access - kCLErrorDenied");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location issue"
                                                        message:@"Your location cannot be determined."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    // NSLog(@"==========viewForAnnotation===========");
    
    // NSLog(@"\n Inside viewForAnnotation @@@@@@@@@@@@@@@@");
    //    MKAnnotationView *pinView = nil;
    //    static NSString *defaultPinID = @"com.invasivecode.pin";
    //    pinView = (MKAnnotationView *)[mkView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    //    if ( pinView == nil ) pinView = [[MKAnnotationView alloc]
    //                                     initWithAnnotation:annotation reuseIdentifier:defaultPinID];
    //
    //    return pinView;
    
    /* if( [[annotation title] isEqualToString:@"Current Location"] )
     {
     return nil;
     }
     if ([annotation isKindOfClass:[MKUserLocation class]]){
     return nil;
     }*/
    if ([[(ProductAnnotation*)annotation title] isEqualToString:@"Current Location"]) {
        
        return nil;
    }
    
    static NSString *annotationIdentifier = @"AnnotationIdentifier";
    
    MKAnnotationView *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
    
    if (annotationView == nil)
        
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifier];
    
    if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"1"]) {
      //  [UIImage imageNamed:@"home_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
        annotationView.image = [UIImage imageNamed:@"automobile_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
        annotationView.annotation = annotation;
    }
    else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"0"]) {
        annotationView.image =  [UIImage imageNamed:@"empty_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
        annotationView.annotation = annotation;
    }
    
    else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"2"]) {
        annotationView.image = [UIImage imageNamed:@"dining_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
        annotationView.annotation = annotation;
    }
    else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"3"]) {
        annotationView.image = [UIImage imageNamed:@"healthAndBeauty_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
        annotationView.annotation = annotation;
    }
    else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"4"]) {
        annotationView.image =  [UIImage imageNamed:@"home_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
        annotationView.annotation = annotation;
    }
    else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"5"]) {
        
        annotationView.image = [UIImage imageNamed:@"shopping_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
        annotationView.annotation = annotation;
    }
    else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"6"]) {
       
        annotationView.image =  [UIImage imageNamed:@"travel_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
        annotationView.annotation = annotation;
    }
    else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"7"]) {
        
        annotationView.image = [UIImage imageNamed:@"leisure.png" inBundle:bundle compatibleWithTraitCollection:nil ];
        annotationView.annotation = annotation;
    }
    else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"8"]) {
        
        annotationView.image = [UIImage imageNamed:@"insurance_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
        annotationView.annotation = annotation;
    }
    else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"9"]) {
       
        annotationView.image =  [UIImage imageNamed:@"golf_map.png" inBundle:bundle compatibleWithTraitCollection:nil ];
        annotationView.annotation = annotation;
    }
    
    
    
    
    annotationView.canShowCallout = YES;
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    annotationView.userInteractionEnabled = YES;
    
    // }
    //else
    // {
    //    annotationView.annotation = annotation;
    // }
    
    return annotationView;
}
- (void)mutateCoordinatesOfClashingAnnotations:(NSArray *)annotations
{
    
    NSDictionary *coordinateValuesToAnnotations = [self groupAnnotationsByLocationValue:annotations];
    NSLog(@"coordinateValuesToAnnotations::%d",[coordinateValuesToAnnotations count]);
    
    for (NSValue *coordinateValue in coordinateValuesToAnnotations.allKeys) {
        NSMutableArray *outletsAtLocation = coordinateValuesToAnnotations[coordinateValue];
        if (outletsAtLocation.count > 1) {
            CLLocationCoordinate2D coordinate;
            [coordinateValue getValue:&coordinate];
            [self repositionAnnotations:outletsAtLocation toAvoidClashAtCoordination:coordinate];
        }
    }
}

- (NSDictionary *)groupAnnotationsByLocationValue:(NSArray *)annotations
{
    
    
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    for (id<MKAnnotation> pin in annotations)
    {
        
        CLLocationCoordinate2D coordinate = pin.coordinate;
        NSValue *coordinateValue = [NSValue valueWithBytes:&coordinate objCType:@encode(CLLocationCoordinate2D)];
        
        NSMutableArray *annotationsAtLocation = result[coordinateValue];
        if (!annotationsAtLocation) {
            annotationsAtLocation = [NSMutableArray array];
            result[coordinateValue] = annotationsAtLocation;
        }
        
        [annotationsAtLocation addObject:pin];
        
        NSLog(@"annotationsAtLocation count is %d ",annotationsAtLocation.count);
        
    }
    return result;
}

- (void)repositionAnnotations:(NSMutableArray *)annotations toAvoidClashAtCoordination:(CLLocationCoordinate2D)coordinate
{
    
    double distance = 3 * annotations.count / 2.0;
    //NSLog(@"distance::%f",distance);
    double radiansBetweenAnnotations = (M_PI * 2) / annotations.count;
    
    for (int i = 0; i < annotations.count; i++) {
        
        double heading = radiansBetweenAnnotations * i;
        CLLocationCoordinate2D newCoordinate = [self calculateCoordinateFrom:coordinate onBearing:heading atDistance:distance];
        
        id <MKAnnotation> annotation = annotations[i];
        annotation.coordinate = newCoordinate;
        
        [mkView addAnnotation:annotations[i]];
    }
    //NSLog(@"annotations.count %d ",annotations.count );
    //[mkView reloadInputViews];
}

- (CLLocationCoordinate2D)calculateCoordinateFrom:(CLLocationCoordinate2D)coordinate  onBearing:(double)bearingInRadians atDistance:(double)distanceInMetres
{
    
    
    double coordinateLatitudeInRadians = coordinate.latitude * M_PI / 180;
    double coordinateLongitudeInRadians = coordinate.longitude * M_PI / 180;
    
    double distanceComparedToEarth = distanceInMetres / 6378100;
    
    double resultLatitudeInRadians = asin(sin(coordinateLatitudeInRadians) * cos(distanceComparedToEarth) + cos(coordinateLatitudeInRadians) * sin(distanceComparedToEarth) * cos(bearingInRadians));
    double resultLongitudeInRadians = coordinateLongitudeInRadians + atan2(sin(bearingInRadians) * sin(distanceComparedToEarth) * cos(coordinateLatitudeInRadians), cos(distanceComparedToEarth) - sin(coordinateLatitudeInRadians) * sin(resultLatitudeInRadians));
    
    CLLocationCoordinate2D result;
    result.latitude = resultLatitudeInRadians * 180 / M_PI;
    result.longitude = resultLongitudeInRadians * 180 / M_PI;
    return result;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    ProductAnnotation *ann =(ProductAnnotation *)view.annotation;
    [self showActivityView];
    [self fetchProductWithProductID:ann.prod.productId];
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    
    
    // NSLog(@"##map region: %f,%f",mapView.region.center.latitude,mapView.region.center.longitude);
    
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    /* CLLocationCoordinate2D loc = [userLocation.location coordinate];
     MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(loc, 1000.0f, 1000.0f);
     [mkView setRegion:region animated:YES];*/
}

//==================================================================================//

- (void) showActivityView {
    activityView.frame = self.view.bounds;
    UIView *loadingLabelView = [activityView viewWithTag:10];
    loadingLabelView.center = activityView.center;
    [self.view addSubview:activityView];
}

- (void) dismissActivityView {
    [activityView removeFromSuperview];
}


#pragma mark -- ASIHttpRequest Delegate methods

- (void)requestFinished:(ASIHTTPRequest *)request {
    
    NSLog(@"NearByVC -- requestFinished:");
    
    if (request == categoryNearByRequest) {
        
        categoryNearByRequest = nil;
        
        NSLog(@"RESULT in NEARBY: %@",[request responseString]);
        NSLog(@"ProductListParser");
        NSXMLParser *productsParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        productsXMLParser = [[ProductListParser alloc] init];
        productsXMLParser.delegate = self;
        productsParser.delegate = productsXMLParser;
        [productsParser parse];
        
    }
    if (request == productFetchRequest) {
        [self dismissActivityView];
        productFetchRequest = nil;
        NSLog(@"Product ** RESULT: %@",[request responseString]);
        NSLog(@"ProductDataParser");
        NSXMLParser *productParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        productDataXMLParser = [[ProductDataParser alloc] init];
        productDataXMLParser.delegate = self;
        productParser.delegate = productDataXMLParser;
        [productParser parse];
    }
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    
    NSLog(@"NearByVC -- requestFailed:");
    
    if (request == categoryNearByRequest) {
        categoryNearByRequest = nil;
    }
    else {
        [self dismissActivityView];
        productFetchRequest = nil;
        UIAlertView *failedAlert=[[UIAlertView alloc]initWithTitle:@"Failed!" message:@"Please check network connection and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [failedAlert show];
    }
    UIAlertView *failedAlert=[[UIAlertView alloc]initWithTitle:@"Failed!" message:@"Please check network connection and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [failedAlert show];
}


#pragma mark -- Product Parser Delegate methods

- (void)parsingProductListFinished:(NSArray *)prodcutsListLocal {
    
    NSLog(@".......... productsListLocal: %d",[prodcutsListLocal count]);
    
    // Here update products list
    if (!productsList) {
        productsList = [[NSMutableArray alloc] initWithArray:prodcutsListLocal];
    }
    else {
        [productsList addObjectsFromArray:prodcutsListLocal];
    }
    [self addNewAnnotations];
}

- (void)parsingProductListXMLFailed {
    
}

#pragma mark -- Product Data Parser Delegate methods

- (void)parsingProductDataFinished:(Product *)product {
    
    prodDetail = product;
    // NSLog(@"\n product offer = %@", prodDetail.productOffer);
    
    if (productController) {
        if ([productController.view superview]) {
            [productController.view removeFromSuperview];
        };
        productController = nil;
    }
    
//    ProductViewController *prVC = [[ProductViewController alloc] initWithNibName:@"ProductViewController" bundle:nil andProduct:prodDetail];
//    prVC.fromNearbyMe = YES;
//    prVC.delegate = self;
//    productController = prVC;
//    [self.navigationController pushViewController:prVC animated:YES];
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:[NSBundle bundleForClass:[self class]]];
    ProductDetailViewController *productDVC = [storyboard instantiateViewControllerWithIdentifier:@"productDetailViewController"];
    productDVC.product_ID = prodDetail.productId;
    [self.navigationController pushViewController:productDVC animated:YES];
    
    
    
    [productController updateUIWithProductDetails:product];
    
}

-(Product *)returnProduct:(Product *)productDet
{
    
    return nil;
}

- (void)parsingProductDataXMLFailed {
    
}


- (void) fetchProductWithProductID:(NSString *) productId {
    
    // Make server call for more products.
    NSString *urlString = [NSString stringWithFormat:@"%@get_product.php?id=%@",URL_Prefix,productId];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"Search URL: %@",urlString);
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    productFetchRequest = [ASIFormDataRequest requestWithURL:url];
    [productFetchRequest setDelegate:self];
    [productFetchRequest startAsynchronous];
    
    
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
