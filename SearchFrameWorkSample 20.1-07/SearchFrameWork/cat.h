//
//  cat.h
//  Coredata
//
//  Created by vairat on 07/07/15.
//  Copyright (c) 2015 Vairat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface cat : NSObject
{
    NSString *catId;
    NSString *catName;
    NSString *catDescription;
    NSString *catImgURLString;
}

@property (nonatomic, strong) NSString *catId;
@property (nonatomic, strong) NSString *catName;
@property (nonatomic, strong) NSString *catDescription;
@property (nonatomic, strong) NSString *catImgURLString;
@end
