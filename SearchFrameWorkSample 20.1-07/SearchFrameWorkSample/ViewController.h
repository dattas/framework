//
//  ViewController.h
//  SearchFrameWorkSample
//
//  Created by vairat on 07/07/15.
//  Copyright (c) 2015 vaisoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SearchFrameWork/SearchFrameWork.h>

@interface ViewController : UIViewController

- (IBAction)pushToView:(id)sender;

@end

