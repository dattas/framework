//
//  ViewController.m
//  SearchFrameWorkSample
//
//  Created by vairat on 07/07/15.
//  Copyright (c) 2015 vaisoft. All rights reserved.
//

#import "ViewController.h"
#import <SearchFrameWork/SearchViewController.h>
#import <SearchFrameWork/sampleViewController.h>
#import <SearchFrameWork/MyCardViewController.h>
#import <MapKit/MapKit.h>
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)viewWillAppear:(BOOL)animated
{
   // SearchViewController *svc=[[SearchViewController alloc]init];
    //[self presentViewController:svc animated:NO completion:nil];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pushToView:(id)sender {
    
    sampleViewController *svc=[[sampleViewController alloc]init];
    //MyCardViewController *svc=[[MyCardViewController alloc]init];
    svc.userID=@"4315412";

    
    [[self navigationController] pushViewController:svc animated:YES];
   // [self presentViewController:svc animated:NO completion:nil];
}
@end
